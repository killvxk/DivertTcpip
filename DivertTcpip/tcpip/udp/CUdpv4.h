#pragma once
class CUdpv4
{
public:
	CUdpv4();
	~CUdpv4();
public:
	bool TxPacket(const uint32_t dstIp, const uint32_t srcIp, const uint16_t dst_port, const uint16_t src_port, const uint8_t *packet, size_t packet_len);
	bool ProcessRxEx(const uint32_t dstIp, const uint32_t srcIp, const uint8_t *packet, size_t packet_len);
};

extern CUdpv4 udpv4;
