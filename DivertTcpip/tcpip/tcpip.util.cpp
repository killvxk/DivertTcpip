#include "stdafx.h"

bool is_zero_ip(uint8_t *_ip)
{
	uint8_t zero[4] = { 0,0,0,0 };
	for (auto i = 0; i < 4; i++)
	{
		if (_ip[i] != zero[i])
			return false;
	}
	return true;
}
bool is_same_mac_address(uint8_t *_addr, uint8_t *_addr2)
{
	for (auto i = 0; i < 6; i++)
	{
		if (_addr2[i]!=_addr[i])
		{
			return false;
		}
	}
	return true;
}

void dumpMac(uint8_t *_mac)
{
	LOG_DEBUG("mac = ");
	for (auto i = 0; i < 6; i++)
	{
		if (i < 5)
			LOG_DEBUG("%02x-", _mac[i]);
		else
			LOG_DEBUG("%02x\r\n", _mac[i]);
	}
}

void dumpIP(uint8_t *ip)
{
	LOG_DEBUG("ip = ");
	for (auto i = 0; i < 4; i++)
	{
		if (i < 3)
			LOG_DEBUG("%d.", ip[i]);
		else
			LOG_DEBUG("%d\r\n", ip[i]);
	}

}

/*
* 对协议头和数据分段进行校验和计算，注意协议头长度必须为偶数
*/
unsigned short rfc_checksum(void* proto_header, int header_len, void *buf, int buf_size)
{
	unsigned long cheksum;
	unsigned short *proto_chk;
	int size;

	/* 计算协议头的长度，要求必须是偶数 */
	if (header_len & 1)
		return 0;

	cheksum = 0;
	for (size = header_len, proto_chk = (unsigned short *)proto_header; size > 1; proto_chk++, size -= 2)
		cheksum += *proto_chk;

	/* 计算数据段checksum */
	if (buf_size > 0) {
		proto_chk = (unsigned short *)buf;
		size = buf_size;
		while (size > 1) {
			cheksum += *proto_chk++;
			size -= 2;
		}

		if (size == 1)
			cheksum += *(unsigned char*)proto_chk;
	}

	cheksum = (cheksum >> 16) + (cheksum & 0xffff);
	cheksum += (cheksum >> 16);

	return (unsigned short)~cheksum;
}

//IP头checksum
unsigned short ip_checksum(struct ip_header* ipheader,unsigned short ip_len)
{
	struct ip_header *ip_hdr;
	unsigned short newsum, savesum;
	if (ipheader == NULL)
		return 0;

	ip_hdr = ipheader;
	if (ip_hdr == NULL)
		return 0;
	savesum = ip_hdr->checksum;
	/* 注意：ip校验和只计算ip头以及选项 */
	ip_hdr->checksum = 0;
	newsum = rfc_checksum(ip_hdr, ip_len, NULL, 0);
	ip_hdr->checksum = savesum;
	return newsum;
}
unsigned short icmp_checksum(struct icmp_header* icmpheader, unsigned short icmp_len)
{
	auto icmp = icmpheader;
	auto save = icmp->checksum;
	icmp->checksum = 0;
	auto sum = rfc_checksum(icmp, icmp_len, NULL, 0);
	icmp->checksum = save;
	return sum;
}

//tcp的checksum
unsigned short tcp_checksum(struct tcp_header* tcpheader,
	uint16_t tcp_header_len,
	struct ip_header*ipheader,
	void *packet,
	size_t packet_len)
{
	//构造头
	uint8_t packet_pad[MAX_PROTO_HEAD] = {};
	//协议头：tcp伪头+tcp头
	//伪头可以在tcp头前后都行，网上大部分人都喜欢在前面
	//这个版本也是在前面的
	auto psd_hdr = reinterpret_cast<tcp_psd_header*>(packet_pad);
	psd_hdr->proto = IP_PROTO_TCP;
	psd_hdr->tcp_len = sock_htons((unsigned short)(tcp_header_len+packet_len));
	psd_hdr->mbz = 0;
	psd_hdr->dst_addr = ipheader->dst_ip;
	psd_hdr->src_addr = ipheader->src_ip;
	auto save = tcpheader->sum;
	tcpheader->sum = 0;
	RtlCopyMemory(packet_pad + sizeof(tcp_psd_header), tcpheader, tcp_header_len);
	auto ret= rfc_checksum((void*)packet_pad, 
		tcp_header_len + sizeof(tcp_psd_header),
		(void*)packet, (int)packet_len);
	tcpheader->sum = save;
	return ret;
}
unsigned short sock_htons(unsigned short data)
{
	return ((data & 0xff00) >> 8) | ((data & 0xff) << 8);
}

unsigned long sock_htonl(unsigned long data)
{
	/*
	unsigned long ret;
	ret = ((data & 0xff)<<24);
	data = data >> 8;
	ret |= ((data & 0xff)<<16);
	data = data >> 8;
	ret |= ((data & 0xff)<<8);
	ret |= data >> 8;
	return ret;
	*/
	return (sock_htons((unsigned short)(data & 0xffff)) << 16) | sock_htons((unsigned short)((data & 0xffff0000) >> 16));
}

int sock_rand()
{
#ifndef _NTDDK_
	static bool init = false;
	if (!init) {
		srand((uint32_t)getTime());
		init = true;
	}
	return rand();
#else
	LARGE_INTEGER tick_count;
	KeQueryTickCount(&tick_count);
	return tick_count.LowPart;
#endif
}

int64_t getTime()
{
#ifndef _NTDDK_
	FILETIME ftime;
	uint64_t time;

	// Time in 100ns ticks
	GetSystemTimeAsFileTime(&ftime);

	time = ftime.dwHighDateTime;
	time <<= 32;
	time += ftime.dwLowDateTime;

	return time;
#else
	LARGE_INTEGER tick_count;
	KeQueryTickCount(&tick_count);
	return tick_count.QuadPart;
#endif
}