#pragma once
class CSocketUDPPool:public Singleton<CSocketUDPPool>
{
public:
	CSocketUDPPool();
	~CSocketUDPPool();
private:
	//��Ҫһ����
	spinlock_mutex _Listlock;
	std::unordered_map<uint16_t,CSocketUDP*>mClientList;
public:
	void freePort(uint16_t port);
	bool registerClient(CSocketUDP *client, uint16_t port);
	bool ProcessRx(IPINFO *ipinfo, uint8_t *_buffer,size_t _length);
};

