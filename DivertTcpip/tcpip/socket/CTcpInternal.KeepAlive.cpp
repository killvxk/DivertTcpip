#include "stdafx.h"

//保活机制的代码
void CSockTcpInternal::init_keep_alive()
{
	kaCount = 0;
	binit_keep_alive = true;
	kaPingTimer = -1000;
	kaTimer = -1000 * 1000 * 60 * 2;
	auto kathread = std::thread(std::bind(
		&CSockTcpInternal::KeepAliveRoutine, this));
	kathread.detach();
}

void CSockTcpInternal::reset_live()
{
	if (!binit_keep_alive)
	{
		init_keep_alive();
	}
	kaCount = 0;
	kaPingTimer = -1000;
	kaTimer = -1000 * 1000 * 60 * 2;
}

void CSockTcpInternal::KeepAliveRoutine()
{
	while (state==ESTABLISHED)
	{
		if (kaEvent.Wait(1))
		{
			InterlockedIncrement(&kaPingTimer);
			if (kaPingTimer==0)
			{
				send_live();
				kaCount++;
			}
			InterlockedIncrement64(&kaTimer);
			if (kaCount > 10)
			{
				onRST(nullptr);
				binit_keep_alive = false;
				return;
			}
			if (kaTimer==0)
			{
				//歇逼了
				onRST(nullptr);
				binit_keep_alive = false;
				return;
			}
		}
		else
		{
			kaEvent.Reset();
			binit_keep_alive = false;
			return;
		}
	}
}

void CSockTcpInternal::exit_keep_alive()
{
	kaEvent.Set();
}

void CSockTcpInternal::send_live()
{
	auto sendSeq = RemoteBlock.Ack-1;
	auto SendAck = RemoteBlock.Seq;
	uint8_t pad[] = { 'S' };
	LOG_DEBUG("keep alive\r\n");
	send_packet(sendSeq, SendAck, pad, 1);
}