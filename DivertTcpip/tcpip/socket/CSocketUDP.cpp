#include "stdafx.h"


CSocketUDP::CSocketUDP()
{
	port = 0;
}


CSocketUDP::~CSocketUDP()
{
	RxEvent.clean();
	if (port)
	{
		CSocketUDPPool::getInstance().freePort(port);
	}
}

bool CSocketUDP::bindport(int nport) {
	port = sock_htons((unsigned short)nport);
	return CSocketUDPPool::getInstance().registerClient(this, port);
}
size_t CSocketUDP::recvfrom(IPINFO *ipinfo, const uint8_t*buffer, size_t wanna_size) {
	if (wanna_size>MAX_RX_BUFFER_SIZE)
	{
		LOG_DEBUG("too big\r\n");
		return size_t(-1);
	}
	//FIXME::实际上我们应该再判断一次ipinfo是否指定了
	size_t ret = 0;
	if (RxEvent.Wait(30000)==false)
	{
		if (!_RxData.empty())
		{
			auto &backRx = _RxData.front();
			if (backRx.RxOffset > backRx.RecvOffset)
			{
				auto p = &backRx.RxBuffer[backRx.RecvOffset];
				auto size = backRx.RxOffset - backRx.RecvOffset;
				uint16_t cpysize = min((uint16_t)wanna_size, size);
				RtlCopyMemory((void *)buffer, (void*)p, cpysize);
				backRx.RecvOffset += cpysize;
				ret = cpysize;
				*ipinfo = backRx.curIpInfo;
				if (backRx.RecvOffset == backRx.RxOffset) {
					_RxData.pop();
				}
			}
		}
		else {
			//队列空了
			RxEvent.Reset();
		}
	}
	return ret;
}
bool CSocketUDP::sendto(IPINFO *dstipinfo, const uint8_t*buffer, size_t _length)
{
	//按照MTU分割
	//ETH 14
	//IP 20
	//UDP 8
	//1500-20-14-8
	/*  
	鉴于Internet上的标准MTU值为576字节，
	所以建议在进行Internet的UDP编程时，
	最好将UDP的数据长度控制在548字节 (576-8-20)以内。*/
	uint8_t udpframe[MAX_UDP_PACKET_SIZE] = {};
	auto count = _length / MAX_UDP_PACKET_SIZE;
	auto remind = _length % MAX_UDP_PACKET_SIZE + 1;
	auto buf = (uint8_t*)buffer;
	for (auto i = 0; i < count; i++)
	{
		if (!udpv4.TxPacket(dstipinfo->remote_ip,
			dstipinfo->local_ip, dstipinfo->remote_port, port,
			(const uint8_t*)(buf+i* MAX_UDP_PACKET_SIZE), MAX_UDP_PACKET_SIZE)) {
			return false;
		}
	}
	
	if (remind != 1)
	{
		//有Remind
		RtlCopyMemory(udpframe, (buf + count * MAX_UDP_PACKET_SIZE), _length % MAX_UDP_PACKET_SIZE);
		if (remind % 2) {
			//奇数
			remind -= 1;
		}
	}
	//我们直接发给Udpv4
	return udpv4.TxPacket(dstipinfo->remote_ip,
		dstipinfo->local_ip, dstipinfo->remote_port, port,
		(const uint8_t*)(udpframe), remind);
}

bool CSocketUDP::RxData(IPINFO *ip, uint8_t *buff, size_t _length)
{
	LOG_DEBUG("RxData udp %lld\r\n", _length);
	if (_length > MAX_RX_BUFFER_SIZE)
	{
		LOG_DEBUG("too long\r\n");
		return false;
	}
	_RXDATA_ data = {};
	data.curIpInfo = *ip;
	data.RecvOffset = 0;
	data.RxOffset = (uint16_t)_length;
	RtlCopyMemory(data.RxBuffer, buff, _length);
	_RxData.push(data);
	RxEvent.Set();
	return true;
}