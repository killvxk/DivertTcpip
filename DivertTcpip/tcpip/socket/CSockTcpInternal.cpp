#include "stdafx.h"


CSockTcpInternal::CSockTcpInternal()
{
	state = CLOSED;
	binit_keep_alive = false;
	binit_rtx = false;
	localport = 0;
	remoteport = 0;
	remoteip = 0;
	bind_value = 0;
	_TcpClient = nullptr;
	MaxSeq = 0;
	OverCount = 0;
	InTxProcessing = false;
	CurTxSize = LastTxSize = 0;
	RtlZeroMemory(&LocalBlock, sizeof(LocalBlock));
	RtlZeroMemory(&RemoteBlock, sizeof(RemoteBlock));
	LocalBlock.RxWnd = MAX_TCP_BUFFER - 1;
	LocalBlock.Mss = MAX_TCP_MSS;
	LocalBlock.CurWnd = 0;
	LocalBlock.Seq = (sock_rand() << 16) + sock_rand();
	LocalBlock.Ack = 0;
	TxState = TX_DONE;
	RtlZeroMemory(&TxBuffer, sizeof(TX_BUFFER));
	localip = *(uint32_t*)Ipv4.getLocalIp();
}


CSockTcpInternal::~CSockTcpInternal()
{
	if (localport)
	{
		CSockTcpPool::getInstance().unregLocalPort(localport, this);
		for (auto &client : clientList)
		{
			delete client;
		}
	}
	if (bind_value)
	{
		CSockTcpPool::getInstance().unregbindAddress(bind_value);
	}
}
void CSockTcpInternal::regTcpClient(CSocketTcp*_client)
{
	if (!_TcpClient && _client) {
		_TcpClient = _client;
		_TcpClient->setLastError(SUCCESS_TCP);
	}
}
bool CSockTcpInternal::BindPort(uint16_t port) {

	localport = port;
	auto bBind = CSockTcpPool::getInstance().regLocalPort(port, this);
	if (bBind)
	{
		_TcpClient->setLastError(SUCCESS_TCP);
	}
	else
	{
		_TcpClient->setLastError(BIND_ALREADY);
	}
	return bBind;
}
bool CSockTcpInternal::Listen() {
	if (!localport)
	{
		_TcpClient->setLastError(NOT_BIND);
		return false;
	}
	if (state != CLOSED) {
		_TcpClient->setLastError(LISTEN_ERROR);
		return false;
	}
	if (state > LISTEN) {
		_TcpClient->setLastError(LISTEN_MODE_CONNECT);
		return false;
	}
	if (state == LISTEN)
	{
		_TcpClient->setLastError(LISTEN_ALREADY);
		return false;
	}
	state = LISTEN;
	_TcpClient->setLastError(SUCCESS_TCP);
	return true;
}
bool CSockTcpInternal::Connect(uint16_t remote_port, uint32_t remote_ip)
{
	if (state > CLOSED)
	{
		_TcpClient->setLastError(CONNECT_ERROR);
		return false;
	}
	remoteport = remote_port;
	remoteip = remote_ip;
	if (localport == 0)
	{
		localport = sock_htons(sock_rand() + 1024);
	}
	uint32_t LrPort = (localport << 16) & 0xFFFF0000;
	LrPort |= remoteport;
	uint64_t Value = (((uint64_t)remoteip) << 32) & 0xFFFFFFFF00000000ui64;
	Value |= LrPort;
	auto b = CSockTcpPool::getInstance().regBindAddress(Value, this);
	if (!b)
	{
		_TcpClient->setLastError(CONNECT_ALREADY);
		return false;
	}
	//这里需要进行SendSYN和三次握手
	auto bsentsyn = send_syn_for_hand();
	if (!bsentsyn)
	{
		_TcpClient->setLastError(TCP_SYN_ERROR);
		CSockTcpPool::getInstance().unregbindAddress(Value);
		return false;
	}
	bind_value = Value;
	_TcpClient->setLastError(SUCCESS_TCP);
	return true;
}
//////////////////////////////////////////////////////////////////////////
CSocketTcp* CSockTcpInternal::getNewClient()
{
	return new CSocketTcp();
}
void CSockTcpInternal::Close()
{
	//发送FIN
	//进入FIN_WAIT_1
	send_fin();
	exit_keep_alive();
	exit_rtx();
	if (!CloseOk.Wait(5000))
		CloseOk.Reset();
	state = CLOSED;
	Clear();
}
void CSockTcpInternal::Clear()
{
	LOG_DEBUG("Clear\r\n");
	CSockTcpPool::getInstance().unregLocalPort(localport, this);
	CSockTcpPool::getInstance().unregbindAddress(bind_value);
	RtlZeroMemory(&LocalBlock, sizeof(LocalBlock));
	RtlZeroMemory(&RemoteBlock, sizeof(RemoteBlock));
	LocalBlock.RxWnd = MAX_TCP_BUFFER - 1;
	LocalBlock.Mss = MAX_TCP_MSS;
	LocalBlock.CurWnd = 0;
	LocalBlock.Seq = (sock_rand() << 16) + sock_rand();
	LocalBlock.Ack = 0;
	RtlZeroMemory(&TxBuffer, sizeof(TX_BUFFER));
}
void CSockTcpInternal::WaitHandleOk()
{
	HandleOk.Wait();
	HandleOk.Reset();
	return;
}
//////////////////////////////////////////////////////////////////////////
void CSockTcpInternal::Accept(uint16_t port, uint32_t ip, TCPPOOL_PACKET *packet)
{
	remoteport = port;
	remoteip = ip;
	uint32_t LrPort = (localport << 16) & 0xFFFF0000;
	LrPort |= remoteport;
	uint64_t Value = (((uint64_t)remoteip) << 32) & 0xFFFFFFFF00000000ui64;
	Value |= LrPort;
	bind_value = Value;
	//先绑定
	CSockTcpPool::getInstance().regBindAddress(Value, this);
	onSYN(packet);//结束SYN
	state = SYN_ACK_SENT;//FSM
}
//////////////////////////////////////////////////////////////////////////
//TCP协议详细处理部分
bool CSockTcpInternal::send_syn_for_hand()
{
	//对remoteip remoteport发送syn，然后state=SYN_SENT
	send_syn();
	state = SYN_SENT;
	for (auto i=0;i<3;i++)
	{
		if (HandleOk.Wait(5000) == false)//15000ms还没有XX
		{
			HandleOk.Reset();
			return true;
		}
		else {
			send_syn();
		}
	}
	
	state = CLOSED;
	return false;
}
//////////////////////////////////////////////////////////////////////////
void CSockTcpInternal::send_syn() {

	LocalBlock.Ack = 0;
	localip = *(uint32_t*)Ipv4.getLocalIp();
	uint8_t packet_pad[MAX_PROTO_HEAD] = {};
	tcp_option_field tcp_opt_mss = {};
	auto iphdr = reinterpret_cast<ip_header*>(packet_pad);
	auto tcphdr = reinterpret_cast<tcp_header*>(packet_pad + 20);
	uint8_t tcpsize = sizeof(tcp_header) + sizeof(tcp_opt_mss);

	tcp_opt_mss.MSSopt.type = TCP_OPT_MSS;
	tcp_opt_mss.MSSopt.len = 4;
	tcp_opt_mss.MSSopt.mss = sock_htons((unsigned short)(LocalBlock.Mss));
	memcpy(tcphdr->opts, &tcp_opt_mss, sizeof(tcp_opt_mss));

	tcphdr->opts[4] = TCP_OPT_NOP;
	tcphdr->opts[5] = TCP_OPT_NOP;
	tcphdr->opts[6] = TCP_OPT_SACK;
	tcphdr->opts[7] = 0x2;

	iphdr->dst_ip = remoteip;
	iphdr->src_ip = localip;
	tcphdr->dst_port = remoteport;
	tcphdr->src_port = localport;
	tcphdr->ack = sock_htonl(LocalBlock.Ack);
	tcphdr->seq = sock_htonl(LocalBlock.Seq);
	tcphdr->urp = 0;

	tcphdr->win = sock_htons(LocalBlock.RxWnd - LocalBlock.CurWnd);
	tcphdr->hlen_flags = (tcpsize >> 2) << 4;
	tcphdr->FLAG_SYN = 1;
	tcphdr->sum = tcp_checksum(tcphdr, tcpsize, iphdr, NULL, 0);

	Ipv4.TxPacket(remoteip, localip, IP_PROTO_TCP, packet_pad + 20, tcpsize);
}
void CSockTcpInternal::send_rst(TCPPOOL_PACKET*_packet)
{
	auto tcpHdr = _packet->tcpheader;
	auto IpHdr = _packet->ipheader;
	auto data_size = _packet->head_size[DATA_BEGIN];
	//LOG_DEBUG("seq= %u\r\n", sock_htonl(tcpHdr->seq));
	auto TxSeq = 0;
	auto TxAck = sock_htonl(tcpHdr->seq) + (data_size ? data_size : 1);
	auto TxIp = IpHdr->src_ip;
	auto TxPort = tcpHdr->src_port;
	auto MyIp = IpHdr->dst_ip;
	//发送RST
	//Seq=0
	//Ack=xseq
	//FLAG.RST=1
	//FLAG.ACK=1
	//Len=0
	uint8_t packet_pad[MAX_RX_BUFFER_SIZE] = {};
	RtlZeroMemory(packet_pad, sizeof(packet_pad));
	//构造包，计算checksum,调用ipv4层发包
	auto TcpHeader = reinterpret_cast<tcp_header*>(packet_pad + 20);
	uint8_t tcpSize = sizeof(tcp_header);
	LOG_DEBUG("%u\r\n", TxAck);
	TcpHeader->ack = sock_htonl(TxAck);
	TcpHeader->seq = sock_htonl(LocalBlock.Seq);
	TcpHeader->urp = 0;
	TcpHeader->dst_port = TxPort;
	TcpHeader->src_port = localport;
	TcpHeader->win = sock_htons(0);
	TcpHeader->hlen_flags = (tcpSize >> 2) << 4;
	TcpHeader->FLAG_RST = 1;
	TcpHeader->FLAG_ACK = 1;
	TcpHeader->sum = tcp_checksum(TcpHeader, tcpSize, IpHdr, NULL, 0);
	Ipv4.TxPacket(TxIp, MyIp, IP_PROTO_TCP, packet_pad + 20, tcpSize);
	//不是服务器发完RST就进入CLOSED
	if (state != LISTEN)
	{
		state = CLOSED;
		Clear();
	}
}
void CSockTcpInternal::send_syn_ack(TCPPOOL_PACKET *packet)
{

	LOG_DEBUG("走到这里\r\n");
	auto IpHdr = packet->ipheader;
	struct tcp_option_field tcp_opt_mss;
	uint8_t packet_pad[MAX_RX_BUFFER_SIZE] = {};
	RtlZeroMemory(packet_pad, sizeof(packet_pad));
	//构造包，计算checksum,调用ipv4层发包
	auto TcpHeader = reinterpret_cast<tcp_header*>(packet_pad);
	uint8_t tcpSize = sizeof(tcp_header) + sizeof(tcp_opt_mss);
	/* 填写MSS选项 */
	tcp_opt_mss.MSSopt.type = TCP_OPT_MSS;
	tcp_opt_mss.MSSopt.len = 4;
	tcp_opt_mss.MSSopt.mss = sock_htons((unsigned short)(LocalBlock.Mss));
	memcpy(TcpHeader->opts, &tcp_opt_mss, sizeof(tcp_opt_mss));

	TcpHeader->opts[4] = TCP_OPT_NOP;
	TcpHeader->opts[5] = TCP_OPT_NOP;
	TcpHeader->opts[6] = TCP_OPT_SACK;
	TcpHeader->opts[7] = 0x2;

	TcpHeader->ack = sock_htonl(LocalBlock.Ack);
	TcpHeader->seq = sock_htonl(++LocalBlock.Seq);
	TcpHeader->urp = 0;
	TcpHeader->dst_port = packet->tcpheader->src_port;
	TcpHeader->src_port = packet->tcpheader->dst_port;
	TcpHeader->win = sock_htons(LocalBlock.RxWnd - LocalBlock.CurWnd);
	TcpHeader->hlen_flags = (tcpSize >> 2) << 4;
	TcpHeader->FLAG_SYN = 1;
	TcpHeader->FLAG_ACK = 1;
	TcpHeader->sum = tcp_checksum(TcpHeader, tcpSize, IpHdr, NULL, 0);
	Ipv4.TxPacket(remoteip, localip, IP_PROTO_TCP, packet_pad, tcpSize);
}
void CSockTcpInternal::send_fin()
{
	localip = *(uint32_t*)Ipv4.getLocalIp();
	uint8_t packet_pad[MAX_PROTO_HEAD] = {};
	auto iphdr = reinterpret_cast<ip_header*>(packet_pad);
	auto tcphdr = reinterpret_cast<tcp_header*>(packet_pad + 20);
	uint8_t tcpsize = sizeof(tcp_header);
	iphdr->dst_ip = remoteip;
	iphdr->src_ip = localip;
	tcphdr->dst_port = remoteport;
	tcphdr->src_port = localport;
	tcphdr->ack = sock_htonl(LocalBlock.Ack);
	tcphdr->seq = sock_htonl(LocalBlock.Seq++);
	tcphdr->urp = 0;
	tcphdr->win = 0;
	tcphdr->hlen_flags = (tcpsize >> 2) << 4;
	tcphdr->FLAG_FIN = 1;
	tcphdr->sum = tcp_checksum(tcphdr, tcpsize, iphdr, NULL, 0);

	Ipv4.TxPacket(remoteip, localip, IP_PROTO_TCP, packet_pad + 20, tcpsize);
}
void CSockTcpInternal::send_ack()
{
	localip = *(uint32_t*)Ipv4.getLocalIp();
	uint8_t packet_pad[MAX_PROTO_HEAD] = {};
	auto iphdr = reinterpret_cast<ip_header*>(packet_pad);
	auto tcphdr = reinterpret_cast<tcp_header*>(packet_pad + 20);
	uint8_t tcpsize = sizeof(tcp_header);
	iphdr->dst_ip = remoteip;
	iphdr->src_ip = localip;
	tcphdr->dst_port = remoteport;
	tcphdr->src_port = localport;
	tcphdr->ack = sock_htonl(LocalBlock.Ack);
	tcphdr->seq = sock_htonl(++LocalBlock.Seq);
	tcphdr->urp = 0;

	tcphdr->win = sock_htons(LocalBlock.RxWnd - LocalBlock.CurWnd);
	tcphdr->hlen_flags = (tcpsize >> 2) << 4;
	tcphdr->FLAG_ACK = 1;
	tcphdr->sum = tcp_checksum(tcphdr, tcpsize, iphdr, NULL, 0);

	Ipv4.TxPacket(remoteip, localip, IP_PROTO_TCP, packet_pad + 20, tcpsize);
}
void CSockTcpInternal::send_fin_ack()
{
	localip = *(uint32_t*)Ipv4.getLocalIp();
	uint8_t packet_pad[MAX_PROTO_HEAD] = {};
	auto iphdr = reinterpret_cast<ip_header*>(packet_pad);
	auto tcphdr = reinterpret_cast<tcp_header*>(packet_pad + 20);
	uint8_t tcpsize = sizeof(tcp_header);
	iphdr->dst_ip = remoteip;
	iphdr->src_ip = localip;
	tcphdr->dst_port = remoteport;
	tcphdr->src_port = localport;
	tcphdr->ack = sock_htonl(LocalBlock.Ack);
	tcphdr->seq = sock_htonl(LocalBlock.Seq++);
	tcphdr->urp = 0;

	tcphdr->win = 0;
	tcphdr->hlen_flags = (tcpsize >> 2) << 4;
	tcphdr->FLAG_FIN = 1;
	tcphdr->FLAG_ACK = 1;
	tcphdr->sum = tcp_checksum(tcphdr, tcpsize, iphdr, NULL, 0);

	Ipv4.TxPacket(remoteip, localip, IP_PROTO_TCP, packet_pad + 20, tcpsize);
}
void CSockTcpInternal::send_ack(uint32_t xseq, uint32_t xack)
{
	localip = *(uint32_t*)Ipv4.getLocalIp();
	uint8_t packet_pad[MAX_PROTO_HEAD] = {};
	auto iphdr = reinterpret_cast<ip_header*>(packet_pad);
	auto tcphdr = reinterpret_cast<tcp_header*>(packet_pad + 20);
	uint8_t tcpsize = sizeof(tcp_header);
	iphdr->dst_ip = remoteip;
	iphdr->src_ip = localip;
	tcphdr->dst_port = remoteport;
	tcphdr->src_port = localport;
	tcphdr->ack = sock_htonl(xack);
	tcphdr->seq = sock_htonl(xseq);
	tcphdr->urp = 0;
	tcphdr->win = sock_htons(LocalBlock.RxWnd - LocalBlock.CurWnd);
	tcphdr->hlen_flags = (tcpsize >> 2) << 4;
	tcphdr->FLAG_ACK = 1;
	tcphdr->sum = tcp_checksum(tcphdr, tcpsize, iphdr, NULL, 0);

	Ipv4.TxPacket(remoteip, localip, IP_PROTO_TCP, packet_pad + 20, tcpsize);
}

void CSockTcpInternal::send_psh()
{
	localip = *(uint32_t*)Ipv4.getLocalIp();
	uint8_t packet_pad[MAX_PROTO_HEAD] = {};
	auto iphdr = reinterpret_cast<ip_header*>(packet_pad);
	auto tcphdr = reinterpret_cast<tcp_header*>(packet_pad + 20);
	uint8_t tcpsize = sizeof(tcp_header);
	iphdr->dst_ip = remoteip;
	iphdr->src_ip = localip;
	tcphdr->dst_port = remoteport;
	tcphdr->src_port = localport;
	tcphdr->ack = sock_htonl(LocalBlock.Ack);
	tcphdr->seq = sock_htonl(LocalBlock.Seq);
	tcphdr->urp = 0;
	tcphdr->win = sock_htons(LocalBlock.RxWnd - LocalBlock.CurWnd);
	tcphdr->hlen_flags = (tcpsize >> 2) << 4;
	tcphdr->FLAG_ACK = 1;
	tcphdr->FLAG_PSH = 1;
	tcphdr->sum = tcp_checksum(tcphdr, tcpsize, iphdr, NULL, 0);

	Ipv4.TxPacket(remoteip, localip, IP_PROTO_TCP, packet_pad + 20, tcpsize);
}
void CSockTcpInternal::send_packet(uint32_t xseq, uint32_t xack, uint8_t*packet, uint16_t size)
{
	localip = *(uint32_t*)Ipv4.getLocalIp();
	uint8_t packet_pad[MAX_RX_BUFFER_SIZE] = {};
	auto iphdr = reinterpret_cast<ip_header*>(packet_pad);
	auto tcphdr = reinterpret_cast<tcp_header*>(packet_pad + 20);
	uint8_t tcpsize = sizeof(tcp_header);
	auto _xpacket = packet_pad + 20 + tcpsize;
	RtlCopyMemory(_xpacket, packet, size);
	iphdr->dst_ip = remoteip;
	iphdr->src_ip = localip;
	tcphdr->dst_port = remoteport;
	tcphdr->src_port = localport;
	tcphdr->ack = sock_htonl(xack);
	tcphdr->seq = sock_htonl(xseq);
	tcphdr->urp = 0;
	tcphdr->win = sock_htons(LocalBlock.RxWnd - LocalBlock.CurWnd);
	tcphdr->hlen_flags = (tcpsize >> 2) << 4;
	tcphdr->FLAG_ACK = 1;
	tcphdr->FLAG_PSH = 1;
	tcphdr->sum = tcp_checksum(tcphdr, tcpsize, iphdr, _xpacket, size);
	add_rtx_packet(packet_pad + 20, tcpsize + size);
	Ipv4.TxPacket(remoteip, localip, IP_PROTO_TCP, packet_pad + 20, tcpsize + size);
}
//////////////////////////////////////////////////////////////////////////
void CSockTcpInternal::onRST(TCPPOOL_PACKET *_packet) {
	LOG_DEBUG("RST \r\n");
	if (state != LISTEN)
	{
		state = CLOSED;
		Clear();
	}
}
void CSockTcpInternal::onSYNACK(TCPPOOL_PACKET *_packet)
{
	LOG_DEBUG("收到SYNACK");

	//收到SynACK，修正MSS
	auto tcpHdr = _packet->tcpheader;
	auto IpHdr = _packet->ipheader;
	auto data_size = _packet->head_size[DATA_BEGIN];
	auto tcp_size = _packet->head_size[TCP_PROTO];
	struct tcp_option_field* tcp_opt_mss = nullptr;

	if (sock_htonl(tcpHdr->ack) != (LocalBlock.Seq + 1))
	{
		LOG_DEBUG("SYNACK ACK Number Wrong\r\n");
		return;
	}
	RemoteBlock.RxWnd = sock_htons(tcpHdr->win);
	RemoteBlock.CurWnd = 0;
	RemoteBlock.Seq = sock_htonl(tcpHdr->seq);
	RemoteBlock.Ack = sock_htonl(tcpHdr->ack);

	if (tcp_size > sizeof(tcp_header))
	{
		//有OPT
		auto opt_size = tcp_size - sizeof(tcp_header);
		for (auto i = 0; i < opt_size;) {
			switch (tcpHdr->opts[i]) {
			case TCP_OPT_MSS:
				if (!tcpHdr->FLAG_SYN)
				{
					LOG_DEBUG("Error\r\n");
					__debugbreak();
					return;
				}
				tcp_opt_mss = (struct tcp_option_field*)(tcpHdr->opts + i);
				RemoteBlock.Mss = sock_ntohs(tcp_opt_mss->MSSopt.mss);
				if (RemoteBlock.Mss < LocalBlock.Mss)
					LocalBlock.Mss = RemoteBlock.Mss;
				i += 4;
				LOG_DEBUG("MSS=%d\r\n", RemoteBlock.Mss);
				break;
			case TCP_OPT_SACK:
				if (!tcpHdr->FLAG_SYN)
				{
					LOG_DEBUG("Error OPT SACK\r\n");
					__debugbreak();
					return;
				};
				i += 2;
				//RemoteBlock.sack_enabled = 1;
				break;
			case TCP_OPT_SACK_EDGE:
				i += tcpHdr->opts[i + 1];
				break;
			case TCP_OPT_NOP:
				i++;
				break;
				/* 以下选项暂时不支持-不处理 */
			case TCP_OPT_SCALE:
				i += 3;
				break;
			case TCP_OPT_TIME:
				i += 10;
				break;
			case TCP_OPT_END:
				break;
			default:
				break;
			}
		}
	}

	LocalBlock.Ack = RemoteBlock.Seq + 1;;
	//返回ACK
	RemoteBlock.Seq++;
	send_ack();
	state = ESTABLISHED;
	HandleOk.Set();
}
void CSockTcpInternal::onACK(TCPPOOL_PACKET *_packet) {
	LOG_DEBUG("进入ACK\r\n");
	auto tcpHdr = _packet->tcpheader;
	auto IpHdr = _packet->ipheader;
	auto data_size = _packet->head_size[DATA_BEGIN];
	auto tcp_size = _packet->head_size[TCP_PROTO];
	auto packet = _packet->head_proto[DATA_BEGIN];
	auto Wnd = sock_htons(tcpHdr->win);
	auto xseq = sock_htonl(tcpHdr->seq);
	auto xack = sock_htonl(tcpHdr->ack);
	auto PSH = tcpHdr->FLAG_PSH;
	auto FIN = tcpHdr->FLAG_FIN;

	//首先checksum
	if (tcpHdr->FLAG_RST)
	{
		//ACK有RST
		LOG_DEBUG("RST\r\n");
		onRST(_packet);
		return;
	}
	//更新RemoteBlock.RxWnd的大小
	RemoteBlock.RxWnd = Wnd;
	
	rtx_esc(xack);
	if (xseq == (RemoteBlock.Seq - 1) && data_size == 1)
	{
		//保活请求发一个ACK，确认活着
		//send_live
		LOG_DEBUG("Live包\r\n");
		send_ack(LocalBlock.Seq, xseq + data_size);
		return;
	}
	if (xack == RemoteBlock.Ack && data_size == 0)
	{
		//ACK for live
		LOG_DEBUG("Live包的ACK\r\n");
		//send_ack(LocalBlock.Seq, xseq + data_size);
		return;
	}

	if (data_size)
	{
		auto checksum = tcp_checksum(tcpHdr, tcp_size, IpHdr, packet, data_size);
		if (checksum != tcpHdr->sum)
		{
			LOG_DEBUG("Fucker\r\n");
			//发retrans给它
			send_ack(LocalBlock.Seq, xseq);
			return;
		}
		//收数据的处理
		//R.SEQ==L.ACK 顺序到达的
		if (xseq == LocalBlock.Ack) {
			LOG_DEBUG("收包\r\n");
			//////////////////////////////////////////////////////////////////////////
			/*接收逻辑1*/
			//在顺序到达上是可以OVERFLOW的
			if (xseq > uint32_t(0xffffffff - data_size))
			{
				OverCount++;
				MaxSeq = xseq + data_size;
			}

			if (LocalBlock.CurWnd + data_size > LocalBlock.RxWnd)
			{
				LocalBlock.CurWnd = LocalBlock.RxWnd;
				send_ack(LocalBlock.Seq, xseq);
			}
			else {
				//顺序接收，刷新R和L
				RemoteBlock.Seq = xseq + data_size;
				RemoteBlock.Ack = xack;
				LocalBlock.Ack = xseq + data_size;
				if (xseq + data_size > MaxSeq)
				{
					MaxSeq = xseq + data_size;
				}
				LocalBlock.CurWnd += data_size;
				_TcpClient->pushRxData(packet, xseq, data_size);
				//回
				send_ack(LocalBlock.Seq, xseq + data_size);
			}
			//////////////////////////////////////////////////////////////////////////
		}
		else {
			LOG_DEBUG("无序处理\r\n");
			//无序的包，等重发，或者Recv时发ReNeed
			if (xseq > RemoteBlock.Seq
				&&xseq < RemoteBlock.Seq + LocalBlock.RxWnd)
			{
				//没有OVER的SEQ随便接收啦
				//发送的R.SEQ大于本地存储SEQ
				//且小于R.SEQ+L.RWND
				//////////////////////////////////////////////////////////////////////////
				/*接收逻辑2*/
				//不能发生OVER
				LOG_DEBUG("乱序接收\r\n");
				if (xseq < uint32_t(0xffffffff - data_size))
				{

					if (LocalBlock.CurWnd + data_size > LocalBlock.RxWnd)
					{
						LocalBlock.CurWnd = LocalBlock.RxWnd;
						send_ack(LocalBlock.Seq, xseq);
					}
					else {
						//因为无序所以不能刷新L
						RemoteBlock.Seq = xseq + data_size;
						RemoteBlock.Ack = xack;
						if (xseq + data_size > MaxSeq)
						{
							MaxSeq = xseq + data_size;
						}
						LocalBlock.CurWnd += data_size;
						_TcpClient->pushRxData(packet, xseq, data_size);
						//回
						send_ack(LocalBlock.Seq, xseq + data_size);
					}
				}
				//////////////////////////////////////////////////////////////////////////
			}
			else {
				//发生Over的情况下，必须等待足够的顺序的
				return;
			}
		}
	}	
	if (PSH)
	{
		_TcpClient->pushRxPointer();
	}
	//ACK的确认工作
	//R.ACK==L.SEQ是正序确认
	//我们的发送是正序发送的
	if (xack == LocalBlock.Seq + LastTxSize) {
		
		//LOG_DEBUG("Tx\r\n");
		//判断发送工作
		//我们的发送工作逻辑：
		/*先Send一个字节，
		然后收到ACK后把后面的按min(R.RWND,L.mss)发
		简单的说就是没有乱序
		直到R.RWND=0;此时我们要等待某次ACK时RWND重新>0
		*/
		if (InTxProcessing)
		{
			//交给Tx逻辑了
			//LOG_DEBUG("Tx\r\n");
			TxProcess();
		}
	}
	else {
		//重发请求
		if (Wnd != 0)
		{
			//重发？
			rtx_packet(xack);
		}
	}
	return;
}
void CSockTcpInternal::onFIN(TCPPOOL_PACKET *_packet)
{
	auto tcpHdr = _packet->tcpheader;
	auto IpHdr = _packet->ipheader;
	auto data_size = _packet->head_size[DATA_BEGIN];
	auto tcp_size = _packet->head_size[TCP_PROTO];

	RemoteBlock.CurWnd += RemoteBlock.RxWnd - sock_htons(tcpHdr->win);
	RemoteBlock.RxWnd = sock_htons(tcpHdr->win);
	RemoteBlock.Seq = sock_htonl(tcpHdr->seq);
	RemoteBlock.Ack = sock_htonl(tcpHdr->ack);
	//发送FIN ACK
	LocalBlock.Ack = RemoteBlock.Seq + data_size;
	send_fin_ack();

	state = CLOSED;
	//调用Clear
	Clear();
}
void CSockTcpInternal::onFINACK(TCPPOOL_PACKET *_packet)
{
	//收到了FINACK
	//发个ACK给对面?想多了
	state = CLOSED;
	//调用Clear
	Clear();
}
void CSockTcpInternal::onSYN(TCPPOOL_PACKET *_packet) {
	LOG_DEBUG("收到SYN\r\n");
	auto tcpHdr = _packet->tcpheader;
	auto IpHdr = _packet->ipheader;
	auto data_size = _packet->head_size[DATA_BEGIN];
	auto tcp_size = _packet->head_size[TCP_PROTO];
	struct tcp_option_field* tcp_opt_mss = nullptr;

	RemoteBlock.RxWnd = sock_htons(tcpHdr->win);
	RemoteBlock.CurWnd = 0;
	RemoteBlock.Seq = sock_htonl(tcpHdr->seq);
	RemoteBlock.Ack = sock_htonl(tcpHdr->ack);
	if (tcp_size > sizeof(tcp_header))
	{
		//有OPT
		auto opt_size = tcp_size - sizeof(tcp_header);
		for (auto i = 0; i < opt_size;) {
			switch (tcpHdr->opts[i]) {
			case TCP_OPT_MSS:
				if (!tcpHdr->FLAG_SYN)
				{
					LOG_DEBUG("Error\r\n");
					__debugbreak();
					return;
				}
				tcp_opt_mss = (struct tcp_option_field*)(tcpHdr->opts + i);
				RemoteBlock.Mss = sock_ntohs(tcp_opt_mss->MSSopt.mss);
				if (RemoteBlock.Mss < LocalBlock.Mss)
					LocalBlock.Mss = RemoteBlock.Mss;
				i += 4;
				LOG_DEBUG("MSS=%d\r\n", RemoteBlock.Mss);
				break;
			case TCP_OPT_SACK:
				if (!tcpHdr->FLAG_SYN)
				{
					LOG_DEBUG("Error OPT SACK\r\n");
					__debugbreak();
					return;
				};
				i += 2;
				//RemoteBlock.sack_enabled = 1;
				break;
			case TCP_OPT_SACK_EDGE:
				i += tcpHdr->opts[i + 1];
				break;
			case TCP_OPT_NOP:
				i++;
				break;
				/* 以下选项暂时不支持-不处理 */
			case TCP_OPT_SCALE:
				i += 3;
				break;
			case TCP_OPT_TIME:
				i += 10;
				break;
			case TCP_OPT_END:
				break;
			default:
				break;
			}
		}
	}
	LocalBlock.Ack = RemoteBlock.Seq + 1;
	LocalBlock.Seq = (sock_rand() << 16) + sock_rand();

	//send_rst(_packet);
	//发SYN ACK
	send_syn_ack(_packet);
}
void CSockTcpInternal::onHandOK(TCPPOOL_PACKET *_packet) {
	LOG_DEBUG("Handle OK\r\n");
	return;
	//ACK过来更新RemoteBlock
	//更新ACK
	//如果有数据要调用onACK
	state = ESTABLISHED;
	HandleOk.Set();
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//收包FSM
void CSockTcpInternal::ProcessRx(TCPPOOL_PACKET *_packet)
{
	auto tcpHdr = _packet->tcpheader;
	auto IpHdr = _packet->ipheader;
	TCPFLAG RxFlag = {};
	RxFlag.hlen_flags = tcpHdr->hlen_flags;
	CSocketTcp*newClient = nullptr;
	auto TxIp = IpHdr->src_ip;
	auto TxPort = tcpHdr->src_port;


	//收到一个包的FSM
	switch (state)
	{
	case CLOSED:
		//处于CLOSED状态
		//后面发RST
		break;
	case LISTEN:
		//收SYN，其他包一样SendRST
		if (RxFlag.FLAG_SYN
			&& !RxFlag.FLAG_CWR
			&& !RxFlag.FLAG_ECE
			&& !RxFlag.FLAG_FIN
			&& !RxFlag.FLAG_PSH
			&& !RxFlag.FLAG_RST
			&& !RxFlag.FLAG_URG
			&& !RxFlag.FLAG_ACK)
		{
			//只能有SYN
			//onSYN(_packet);
			//state = SYN_RCVD;
			////NewClient走起
			newClient = getNewClient();
			if (newClient)
			{
				clientList.push_back(newClient);
				newClient->TcpBind(sock_htons(localport));
				newClient->Accept(TxPort, TxIp, _packet);//发syn ack mss max_win给对面
				_TcpClient->pushClient(newClient);//TcpSocket的Accpet就是从队列上返回一个Client
				return;
			}
		}
		break;
	case SYN_ACK_SENT:
	{
		if (!RxFlag.FLAG_SYN
			&& !RxFlag.FLAG_CWR
			&& !RxFlag.FLAG_ECE
			&& !RxFlag.FLAG_FIN
			&& !RxFlag.FLAG_PSH
			&& !RxFlag.FLAG_RST
			&& !RxFlag.FLAG_URG
			&& RxFlag.FLAG_ACK)
		{
			onHandOK(_packet);
			return;
		}
		if (RxFlag.FLAG_SYN)
		{
			onSYN(_packet);
		}
		return;
	}
	break;
	case SYN_SENT:
		//期待收到SYN ACK
		if (RxFlag.FLAG_SYN
			&& !RxFlag.FLAG_CWR
			&& !RxFlag.FLAG_ECE
			&& !RxFlag.FLAG_FIN
			&& !RxFlag.FLAG_PSH
			&& !RxFlag.FLAG_RST
			&& !RxFlag.FLAG_URG
			&& RxFlag.FLAG_ACK)
		{
			onSYNACK(_packet);
			return;
		}
		break;
	case SYN_RCVD:
		if (!RxFlag.FLAG_SYN
			&& !RxFlag.FLAG_CWR
			&& !RxFlag.FLAG_ECE
			&& !RxFlag.FLAG_FIN
			&& !RxFlag.FLAG_PSH
			&& !RxFlag.FLAG_RST
			&& !RxFlag.FLAG_URG
			&& RxFlag.FLAG_ACK)
		{
			onHandOK(_packet);

			return;
		}
		if (RxFlag.FLAG_RST)
		{
			//客户段歇逼
			onRST(_packet);
			return;
		}
		if (RxFlag.FLAG_FIN)
		{
			onFIN(_packet);
			return;
		}
		return;
		break;
	case ESTABLISHED:
		//ACK,FIN,RST处理
		reset_live();//KeepAlive机制
		if (RxFlag.FLAG_RST)
		{
			onRST(_packet);
			return;
		}
		if (RxFlag.FLAG_ACK)
		{
			onACK(_packet);
		}
		if (RxFlag.FLAG_FIN)
		{
			onFIN(_packet);
			return;
		}
		return;
		break;
	default:
		break;
	}
	send_rst(_packet);
}

void CSockTcpInternal::resizeWnd(size_t size) {
	LocalBlock.ReSize += (uint32_t)size;
	if (LocalBlock.ReSize >= (LocalBlock.Mss/2))
	{
		if (LocalBlock.ReSize >= LocalBlock.CurWnd)
			LocalBlock.CurWnd = 0;
		else
			LocalBlock.CurWnd -= LocalBlock.ReSize;
		LocalBlock.ReSize = 0;
	}
	return;

}

void CSockTcpInternal::put_reneed(uint32_t seq)
{
	LOG_DEBUG("ReNeed\r\n");
	//立即模式
	for (auto i=0;i<4;i++)
	{
		//4次SEQ的ACK
		send_ack(LocalBlock.Seq, seq);
	}
}