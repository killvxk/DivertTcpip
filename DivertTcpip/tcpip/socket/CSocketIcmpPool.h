#pragma once
class CSocketIcmpPool :public Singleton<CSocketIcmpPool>
{
public:
	CSocketIcmpPool();
	~CSocketIcmpPool();
private:
	uint16_t _id;
	uint16_t _seq;
	spinlock_mutex _lock;
	HANDLE _init_handle;
	std::unordered_map<HANDLE,CSocketIcmp*>m_Socketlist;
public:
	void unregClass(HANDLE _handle);
	HANDLE regClass(CSocketIcmp *_this, uint32_t handle);
	void get_idseq(uint16_t &xid, uint16_t &seq);
	bool processRx(uint32_t remoteip,uint16_t xid,uint16_t xseq,uint8_t ttl);
};

