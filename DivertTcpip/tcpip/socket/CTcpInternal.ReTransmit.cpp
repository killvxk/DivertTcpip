#include "stdafx.h"

//超时重传机制与重传
void CSockTcpInternal::rtx_packet(uint32_t _seq)
{
	//找到那个包然后interlock count
	if (binit_rtx)
	{
		//LOG_DEBUG("rtx = %u\r\n", _seq);
		RTX_PACKET *findp=nullptr;
		bool find = false;
		RtxLock.lock();
		auto ptr = RtxMap.find(_seq);
		if (ptr!=RtxMap.end())
		{
			//更新次数与时间
			ptr->second.ack_count++;
			ptr->second.time_us = getTime();
			if (ptr->second.ack_count >= 3)
			{
				find = true;
				findp = &ptr->second;
			}
		}
		RtxLock.unlock();
		if (find)
		{
			//重发
			LOG_DEBUG("ReSent\r\n");
			Ipv4.TxPacket(remoteip, localip, IP_PROTO_TCP, findp->packet, findp->size);
		}
	}
}
void CSockTcpInternal::init_rtx()
{
	binit_rtx = true;
	//ntdll::InitializeListHead(&RtxWorkList);
	auto x = std::thread(std::bind(
		&CSockTcpInternal::rtx_routine, this));
	x.detach();
}

void CSockTcpInternal::add_rtx_packet(uint8_t *packet, uint16_t _size)
{
	if (!binit_rtx)
	{
		init_rtx();
	}
	if (binit_rtx)
	{
		//开始干活
		auto Tcphdr = reinterpret_cast<tcp_header *>(packet);
		auto Seq = sock_htonl(Tcphdr->seq) + _size - 20;
		//LOG_DEBUG("add rx %u\r\n", Seq);
		RTX_PACKET new_rtx = {};
		RtlZeroMemory(&new_rtx, sizeof(new_rtx));
		new_rtx.size = _size;
		RtlCopyMemory(new_rtx.packet, packet, _size);
		RtxLock.lock();
		new_rtx.time_us = getTime();
		RtxMap[Seq] = new_rtx;
		RtxLock.unlock();
	}
}
void CSockTcpInternal::rtx_routine()
{
	while (ESTABLISHED==state)
	{
		if (RtxEvent.Wait(1))
		{
			auto now_us = getTime();
			RtxLock.lock();
			if (!RtxMap.empty()) {
				for (auto &ptr:RtxMap)
				{
					auto pPacket = &ptr.second;
					auto old = pPacket->time_us;
					auto delta = (now_us - old) / 1000;

					if (delta > 5000)
					{
						pPacket->time_us = now_us;
						Ipv4.TxPacket(remoteip, localip, IP_PROTO_TCP, pPacket->packet, pPacket->size);
					}
				}
			}
			RtxLock.unlock();
		}
		else {
			RtxEvent.Reset();
			RtxLock.lock();
			RtxMap.clear();
			RtxLock.unlock();
			binit_rtx = false;
			return;
		}
	}
	binit_rtx = false;
}

void CSockTcpInternal::rtx_esc(uint32_t _seq)
{
	//取消
	//LOG_DEBUG("will del rx seq=%u\r\n", _seq);
	RtxLock.lock();
	auto ptr = RtxMap.find(_seq);
	if (ptr != RtxMap.end())
	{
		//LOG_DEBUG("del rx seq=%u\r\n", _seq);
		RtxMap.erase(ptr);
	}
	//else {
	//	//LOG_DEBUG("not find rx seq=%u\r\n", _seq);
	//}
	RtxLock.unlock();
}

void CSockTcpInternal::exit_rtx()
{
	RtxEvent.Set();
	RtxMap.clear();
	binit_rtx = false;
	return;
}