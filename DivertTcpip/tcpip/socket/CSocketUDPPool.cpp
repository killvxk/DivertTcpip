#include "stdafx.h"


CSocketUDPPool::CSocketUDPPool()
{
}


CSocketUDPPool::~CSocketUDPPool()
{
}


bool CSocketUDPPool::registerClient(CSocketUDP *client, uint16_t port) {
	bool find = false;
	_Listlock.lock();
	auto ptr = mClientList.find(port);
	if (ptr != mClientList.end())
		find = true;
	if (!find)
	{
		mClientList[port] = client;
	}
	_Listlock.unlock();
	return !find;
}
bool CSocketUDPPool::ProcessRx(IPINFO *ipinfo, uint8_t *_buffer, size_t _length) {
	//LOG_DEBUG("UDP Pool\r\n");
	CSocketUDP*_udpClient = nullptr;
	bool find = false;
	_Listlock.lock();
	auto ptr = mClientList.find(ipinfo->local_port);
	if (ptr != mClientList.end())
	{
		find = true;
		_udpClient = ptr->second;
	}
	_Listlock.unlock();
	if (find)
	{
		return _udpClient->RxData(ipinfo, _buffer, _length);
	}
	return false;
}

void CSocketUDPPool::freePort(uint16_t port)
{
	_Listlock.lock();
	auto ptr = mClientList.find(port);
	if (ptr!=mClientList.end())
	{
		mClientList.erase(ptr);
	}
	_Listlock.unlock();
}