#pragma once
enum SOCKET_TCP_ERROR
{
	SUCCESS_TCP=0,
	NOT_BIND=-10000,
	BIND_ALREADY,
	LISTEN_ERROR,
	LISTEN_ALREADY,
	LISTEN_MODE_CONNECT,
	SEND_ERROR,
	RECV_ERROR,
	RECV_NO_DATA,
	CONNECT_ERROR,
	CONNECT_ALREADY,
	TCP_SYN_ERROR,
	TCP_RST,
	NOT_ESTABLISHED,
};

class CSocketTcp
{
public:
	CSocketTcp();
	~CSocketTcp();
private:
	int16_t lastError;
	uint16_t local_port;
	CSockTcpInternal TcpInternal;
	std::vector<RX_BUFFER> RxBuffer;//无限大小的
	std::vector<uint8_t>_RxByteBuffer;
	size_t ReadByte;
	spinlock_mutex _RxLock;
	osEvent RxWait;
	std::queue<CSocketTcp*>_newList;
public:
	void pushRxData(uint8_t *data,uint32_t index,uint16_t _size);
	void pushRxPointer();
	int16_t getLastError();
	void setLastError(int16_t error_code);
	CSockTcpInternal *getInternal() {
		return &TcpInternal;
	}
	void pushClient(CSocketTcp*_newClient) {
		_newList.push(_newClient);
	}
	void WaitOK();
	void Accept(uint16_t port, uint32_t ip, TCPPOOL_PACKET *packet);
public:
	bool TcpBind(int nport);
	bool TcpConnect(uint32_t ipaddr, int nport);
	bool TcpLisent();
public:
	CSocketTcp*Accept();
	size_t blockSend(const uint8_t *buffer, size_t buffersize);
	size_t blockRecv(uint8_t *buffer, size_t buffersize);
	void Clear();//TcpInternal断开连接走起
	//异步
	//bool asyncSend(const uint8_t *buffer, size_t buffersize, std::function<void(int16_t, size_t)> _onSendDone);
	//bool asynRecv(size_t max_recv_size,std::function<void(uint8_t*, size_t)>_onRecv);
};

