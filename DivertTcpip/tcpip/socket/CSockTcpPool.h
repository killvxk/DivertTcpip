#pragma once
class CSockTcpPool:public Singleton<CSockTcpPool>
{
public:
	CSockTcpPool();
	~CSockTcpPool();
private:
	spinlock_mutex Lock_TcpInternal;
	spinlock_mutex Lock_LocalInternal;
	spinlock_mutex Lock_RxPool;
	std::queue<TCPPOOL_PACKET*>RxPool;
	osEvent RxEvent;
	std::unordered_map<uint64_t, CSockTcpInternal*>m_TcpInternal;
	std::unordered_map<uint16_t, CSockTcpInternal*>m_LocalTcpInternal;
private:
	void PspRx(TCPPOOL_PACKET *tcp_packet);
	void PspiRx(ip_header*ip, uint16_t ip_len, uint8_t*packet, size_t packet_size);
	void PoolRxLoop();
public:
	bool regLocalPort(uint16_t port, CSockTcpInternal *_client);
	bool regBindAddress(uint64_t value, CSockTcpInternal*_client);
public:
	void unregbindAddress(uint64_t value);
	void unregLocalPort(uint16_t port,CSockTcpInternal *_client);
public:
	void RxProcess(ip_header*ip, uint16_t ip_len, uint8_t*packet, size_t packet_size);
};

