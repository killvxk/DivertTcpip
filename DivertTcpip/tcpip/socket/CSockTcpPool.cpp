#include "stdafx.h"


CSockTcpPool::CSockTcpPool()
{
	auto t1 = std::thread(std::bind(&CSockTcpPool::PoolRxLoop, this));
	t1.detach();
}


CSockTcpPool::~CSockTcpPool()
{
}

void CSockTcpPool::PoolRxLoop()
{
	while (RxEvent.Wait())
	{
		while (!RxPool.empty())
		{
			Lock_RxPool.lock();
			auto packet = RxPool.front();
			RxPool.pop();
			Lock_RxPool.unlock();
			PspRx(packet);
			free(packet);
		}
		RxEvent.Reset();
	}
}
void CSockTcpPool::RxProcess(ip_header*ip, uint16_t ip_len, uint8_t*packet, size_t packet_size)
{
	//LOG_DEBUG("Tcp RxProcess\r\n");
	//LOG_DEBUG("ip_len = %d\r\n", ip_len);
	if (packet_size > MAX_RX_BUFFER_SIZE)
	{
		LOG_DEBUG("so big tcppacket\r\n");
		/*直接交给PspX处理*/
		PspiRx(ip, ip_len, packet, packet_size);
		return;
	}
	auto new_packet = (TCPPOOL_PACKET *)malloc(sizeof(TCPPOOL_PACKET));
	if (!new_packet)
	{
		LOG_DEBUG("this\r\n");
		//失败了
		PspiRx(ip, ip_len, packet, packet_size);
		return;
	}
	//LOG_DEBUG("this0\r\n");
	RtlCopyMemory(new_packet->packet, packet, packet_size);
	//LOG_DEBUG("this1\r\n");
	new_packet->head_proto[IP_PROTO] = new_packet->packet;
	//LOG_DEBUG("this2\r\n");
	new_packet->ipheader = (ip_header *)new_packet->head_proto[IP_PROTO];
	//LOG_DEBUG("this3\r\n");
	new_packet->head_proto[TCP_PROTO] = new_packet->head_proto[IP_PROTO] + ip_len;
	//LOG_DEBUG("this4\r\n");
	new_packet->tcpheader = (tcp_header *)(new_packet->head_proto[TCP_PROTO]);
	//LOG_DEBUG("this5\r\n");
	//LOG_DEBUG("seq=%x ack=%x\r\n", sock_htonl(new_packet->tcpheader->seq), sock_htonl(new_packet->tcpheader->ack));
	//LOG_DEBUG("port=%d\r\n", sock_htons(new_packet->tcpheader->dst_port));

	auto tcp_head_size = (new_packet->tcpheader->len_res >> 4) << 2;
	auto all_data_size = sock_htons(ip->len);
	new_packet->head_proto[DATA_BEGIN] = new_packet->head_proto[TCP_PROTO] + tcp_head_size;
	new_packet->head_size[IP_PROTO] = ip_len;
	new_packet->head_size[TCP_PROTO]= tcp_head_size;
	new_packet->head_size[DATA_BEGIN] = all_data_size - ip_len - tcp_head_size;
	//LOG_DEBUG("this6");
	Lock_RxPool.lock();
	RxPool.push(new_packet);
	Lock_RxPool.unlock();
	RxEvent.Set();
}
void CSockTcpPool::PspiRx(ip_header*ip, uint16_t ip_len, uint8_t*packet, size_t packet_size)
{
	if (packet_size + ip_len > MAX_RX_BUFFER_SIZE)
	{
		LOG_DEBUG("too big\r\n");
		return;
	}
	
	TCPPOOL_PACKET tcppacket = {};
	auto new_packet = &tcppacket;
	RtlCopyMemory(new_packet->packet , packet, packet_size);
	new_packet->head_proto[IP_PROTO] = new_packet->packet;
	new_packet->ipheader = (ip_header *)new_packet->head_proto[IP_PROTO];
	new_packet->head_proto[TCP_PROTO] = new_packet->packet + ip_len;
	new_packet->tcpheader = (tcp_header *)(new_packet->head_proto[TCP_PROTO]);
	auto tcp_head_size = (new_packet->tcpheader->len_res >> 4) << 2;
	auto all_data_size = sock_htons(ip->len);
	new_packet->head_proto[DATA_BEGIN] = new_packet->head_proto[TCP_PROTO] + tcp_head_size;
	new_packet->head_size[IP_PROTO] = ip_len;
	new_packet->head_size[TCP_PROTO] = tcp_head_size;
	new_packet->head_size[DATA_BEGIN] = all_data_size - ip_len - tcp_head_size;

	PspRx(&tcppacket);
}

void CSockTcpPool::PspRx(TCPPOOL_PACKET *tcp_packet)
{
	//开始干货
	//取remoteip
	//remoteport
	//localport
	bool find = false;
	auto iphdr = tcp_packet->ipheader;
	auto tcphdr = tcp_packet->tcpheader;
	auto remoteip = iphdr->src_ip;
	auto localport = tcphdr->dst_port;
	auto remoteport = tcphdr->src_port;
	uint32_t LrPort = (localport<<16)&0xFFFF0000;
	LrPort |= remoteport;
	uint64_t Value = (((uint64_t)remoteip) << 32)&0xFFFFFFFF00000000ui64;
	Value |= LrPort;
	//LOG_DEBUG("lr=%08x\r\n", LrPort);
	//LOG_DEBUG("v=%16llx\r\n", Value);
	CSockTcpInternal*_internalClient = nullptr;

	Lock_TcpInternal.lock();
	auto ptr = m_TcpInternal.find(Value);
	if (ptr != m_TcpInternal.end())
	{
		_internalClient = ptr->second;
	}
	Lock_TcpInternal.unlock();
	if (!_internalClient)
	{
		Lock_LocalInternal.lock();
		auto ptr1 = m_LocalTcpInternal.find(localport);
		if (ptr1 != m_LocalTcpInternal.end())
		{
			_internalClient = ptr1->second;
		}
		Lock_LocalInternal.unlock();
	}
	if (_internalClient)
	{
		//把包交给具体的internal
		_internalClient->ProcessRx(tcp_packet);
	}
}

bool CSockTcpPool::regLocalPort(uint16_t port, CSockTcpInternal *_client)
{
	bool bfind = false;
	Lock_LocalInternal.lock();
	auto ptr = m_LocalTcpInternal.find(port);
	if (ptr != m_LocalTcpInternal.end())
	{
		bfind = true;
	}
	Lock_LocalInternal.unlock();
	if (!bfind) {
		m_LocalTcpInternal[port] = _client;
		return true;
	}
	return false;
}
bool CSockTcpPool::regBindAddress(uint64_t value, CSockTcpInternal*_client)
{
	bool bfind = false;
	Lock_TcpInternal.lock();
	auto ptr = m_TcpInternal.find(value);
	if (ptr != m_TcpInternal.end())
	{
		bfind = true;
	}
	Lock_TcpInternal.unlock();
	if (!bfind)
	{
		m_TcpInternal[value] = _client;
		return true;
	}
	return false;
}

void CSockTcpPool::unregbindAddress(uint64_t value)
{
	Lock_TcpInternal.lock();
	auto ptr = m_TcpInternal.find(value);
	if (ptr != m_TcpInternal.end())
	{
		m_TcpInternal.erase(ptr);
	}
	Lock_TcpInternal.unlock();
	return;
}
void CSockTcpPool::unregLocalPort(uint16_t port,CSockTcpInternal*_client)
{
	Lock_LocalInternal.lock();
	auto ptr = m_LocalTcpInternal.find(port);
	if (ptr != m_LocalTcpInternal.end())
	{
		if(ptr->second==_client)
			m_LocalTcpInternal.erase(ptr);
	}
	Lock_LocalInternal.unlock();
}