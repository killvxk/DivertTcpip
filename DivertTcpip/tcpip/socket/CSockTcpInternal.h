#pragma once
class CSocketTcp;
class CSockTcpInternal
{
public:
	CSockTcpInternal();
	~CSockTcpInternal();
	friend class CSocketTcp;
private:
	uint16_t localport;
	uint16_t remoteport;
	uint32_t remoteip;
	uint32_t localip;
	uint64_t bind_value;
private:
	TCP_STATE state;
	TX_STATE TxState;
	uint32_t MaxSeq;
	uint32_t OverCount;
	TCP_BLOCK LocalBlock;
	TCP_BLOCK RemoteBlock;
private:
	osEvent HandleOk;
	osEvent CloseOk;
	osEvent TxDone;
	spinlock_mutex TxLock;
	TX_BUFFER TxBuffer;
	bool InTxProcessing;
	uint16_t CurTxSize;
	uint16_t LastTxSize;
private:
	spinlock_mutex Lock_clientlist;
	std::vector<CSocketTcp*>clientList;//server模式时这些Client绑定其他的internel
	CSocketTcp*_TcpClient;//发起bind的那个
private:
	void resizeWnd(size_t size);
	void put_reneed(uint32_t seq);
private:
	bool send_syn_for_hand();
	void send_psh();
	void send_rst(TCPPOOL_PACKET*_packet);
	void send_fin();
	void send_syn();
	void send_syn_ack(TCPPOOL_PACKET *packet);
	void send_ack();
	void send_fin_ack();
	void send_ack(uint32_t xseq, uint32_t xack);
	void send_packet(uint32_t xseq, uint32_t xack, uint8_t*packet, uint16_t size);
private:
	//保活机制 lwip的2小时生命问题，自己保活或者对面保活
	//先定义一波
	bool binit_keep_alive;
	osEvent kaEvent;
	uint32_t kaCount;
	LONG kaPingTimer;
	LONGLONG kaTimer;
	//ESTABLISHED状态下，如果1000ms内没有任何包到达，则进行保活，否则时间重置
	//如果2个小时没有任何包，则本机进入CLOSED,否则时间重置
	//KAT==2*60*1000*1000ms
	//KAPT=1000ms到15000ms
	void send_live();
	void reset_live();
	void KeepAliveRoutine();
	void init_keep_alive();
	void exit_keep_alive();
private:
	//超时重发
	struct RTX_PACKET
	{
		//这是一个完整的TCP包
		uint8_t packet[MAX_RX_BUFFER_SIZE];
		uint16_t size;
		uint64_t time_us;
		uint8_t ack_count;
	};
	bool binit_rtx;
	osEvent RtxEvent;
	spinlock_mutex RtxLock;
	std::unordered_map<uint32_t, RTX_PACKET>RtxMap;
	void rtx_routine();
	void rtx_packet(uint32_t _seq);
	void rtx_esc(uint32_t _seq);
	void add_rtx_packet(uint8_t *packet, uint16_t _size);
	void init_rtx();
	void exit_rtx();
private:
	void Clear();
private:
	void onSYNACK(TCPPOOL_PACKET *_packet);
	void onRST(TCPPOOL_PACKET *_packet);
	void onHandOK(TCPPOOL_PACKET *_packet);
	void onFIN(TCPPOOL_PACKET *_packet);
	void onFINACK(TCPPOOL_PACKET *_packet);
	void onACK(TCPPOOL_PACKET *_packet);
	void onSYN(TCPPOOL_PACKET *_packet);
private:
	CSocketTcp*getNewClient();
public:
	void ProcessRx(TCPPOOL_PACKET *_packet);
private://注册口
	void regTcpClient(CSocketTcp*_client);
public:
	bool BindPort(uint16_t port);
	bool Listen();
private:
	void Accept(uint16_t port,uint32_t ip,TCPPOOL_PACKET *packet);
	void Close();
	void WaitHandleOk();
public:
	bool Connect(uint16_t remote_port, uint32_t remote_ip);
private:
	uint16_t TxPacket(uint8_t*buffer, uint16_t buffer_size);
	void TxProcess();
	
};

