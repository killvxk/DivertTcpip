#pragma once
class CSocketIcmp
{
public:
	CSocketIcmp();
	~CSocketIcmp();
private:
	uint16_t seq;
	uint16_t id;
	uint32_t time_send;
	uint32_t time_recv;
	uint8_t ttl;
	uint32_t ip;
	HANDLE hHanlde;
	osEvent xxEvent;
public:
	void ping_ip(uint32_t xip);
	void get_reply(uint8_t &xttl, uint32_t &xtime);
	void ProcessRx(uint8_t _ttl, uint32_t _time,uint32_t src_ip);
};

