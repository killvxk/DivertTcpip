#include "stdafx.h"

uint16_t CSockTcpInternal::TxPacket(uint8_t*buffer, uint16_t buffer_size)
{
	if (RemoteBlock.RxWnd == 0)
		return 0;
	//前面Buffer_size已经在TcpClient里分隔好了
	//数据传输，塞入TxBuffer
	TxLock.lock();
	RtlCopyMemory(TxBuffer.Buffer, buffer, buffer_size);
	TxBuffer.size = buffer_size;
	uint16_t minSize = min(RemoteBlock.RxWnd, LocalBlock.Mss);
	if (RemoteBlock.RxWnd == 0)
		minSize = 1;
	CurTxSize = 0;
	InTxProcessing = true;
	uint16_t real_send_size = min(minSize, TxBuffer.size - CurTxSize);
	auto send_seq = LocalBlock.Seq;
	send_packet(send_seq, RemoteBlock.Seq, TxBuffer.Buffer + CurTxSize, real_send_size);
	LastTxSize = real_send_size;
	TxLock.unlock();
	if (TxDone.Wait(5000)==false){
		TxDone.Reset();
	}
	TxLock.lock();
	InTxProcessing = false;
	TxLock.unlock();
	return CurTxSize;
}

void CSockTcpInternal::TxProcess()
{
	//从TxBuffer取数据,然后发送
	uint16_t minSize = min(RemoteBlock.RxWnd, LocalBlock.Mss);
	if (RemoteBlock.RxWnd==0)
		minSize = 1;
	TxLock.lock();
	if (InTxProcessing)
	{
		CurTxSize += LastTxSize;
		LocalBlock.Seq += LastTxSize;
		if (CurTxSize == TxBuffer.size)
		{
			InTxProcessing = false;
			TxDone.Set();
		}
		else {
			LOG_DEBUG("Send Tx Buffer\r\n");
			uint16_t real_send_size = min(minSize, TxBuffer.size - CurTxSize);
			auto send_seq = LocalBlock.Seq;
			send_packet(send_seq, RemoteBlock.Seq, TxBuffer.Buffer + CurTxSize, real_send_size);
			LastTxSize = real_send_size;
		}
	}
	TxLock.unlock();
}