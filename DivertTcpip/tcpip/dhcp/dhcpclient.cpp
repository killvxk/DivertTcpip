#include "stdafx.h"

CSocketUDP dhcpClient;
bool initdhcp = false;
static const uint32_t DHCP_MAGIC = 0x63825363;
AddressInfo ipv4Data = {};
uint8_t myMac[6] = { 'B','A','D','B','O','Y' };
uint32_t PendingXID = 0;
void Discover();
void dhcp_recv();
void SendDhcpRequest(uint8_t *RequestIp);
void init_dhcpclient()
{
	if (!initdhcp)
	{
		dhcpClient.bindport(DHCP_SOURCE_PORT);
		auto thread = std::thread(Discover);
		thread.detach();
		auto thread2 = std::thread(dhcp_recv);
		thread2.detach();
		initdhcp = true;
	}
}

void Discover()
{
	while (is_zero_ip((uint8_t *)Ipv4.getLocalIp()))
	{
		Sleep(3000);
		uint8_t packet_pad[MAX_UDP_PACKET_SIZE] = {};
		RtlZeroMemory(packet_pad, sizeof(packet_pad));
		auto dhcp_hdr = reinterpret_cast<DHCP_PACKET*>(packet_pad);
		auto packsize = sizeof(DHCP_PACKET) - 4;
		PendingXID = (uint32_t)getTime();
		dhcp_hdr->op = BOOTREQUEST;
		dhcp_hdr->htype = HTYPE_ETHER;
		dhcp_hdr->hlen = ETH_ALEN;
		dhcp_hdr->hop = 0;
		dhcp_hdr->xid = sock_htonl(PendingXID);
		dhcp_hdr->nsec = 0;
		dhcp_hdr->flags = sock_htons(BOOTP_BROADCAST);
		dhcp_hdr->ciaddr = 0;
		dhcp_hdr->yiaddr = 0;
		dhcp_hdr->siaddr = 0;
		dhcp_hdr->giaddr = 0;
		for (auto i = 0; i < 6; i++)
		{
			dhcp_hdr->hard_address[i] = myMac[i];
		}
		dhcp_hdr->magic_dhcp = sock_htonl(DHCP_MAGIC);
		auto opindex = 0;
		dhcp_hdr->options[opindex++] = DHO_DHCP_MESSAGE_TYPE;
		dhcp_hdr->options[opindex++] = 1;
		dhcp_hdr->options[opindex++] = DHCPDISCOVER;// DHCP Discover
		dhcp_hdr->options[opindex++] = DHO_DHCP_CLIENT_IDENTIFIER;
		dhcp_hdr->options[opindex++] = 7;
		dhcp_hdr->options[opindex++] = HTYPE_ETHER;
		for (auto i = 0; i < 6; i++)
			dhcp_hdr->options[opindex++] = myMac[i];
		const char host_name[] = "myXTcp";
		dhcp_hdr->options[opindex++] = 12;
		dhcp_hdr->options[opindex++] = (uint8_t)strlen(host_name);
		for (auto i = 0; i < strlen(host_name); i++)
		{
			dhcp_hdr->options[opindex++] = host_name[i];
		}
		dhcp_hdr->options[opindex++] = DHO_DHCP_PARAMETER_REQUEST_LIST;
		dhcp_hdr->options[opindex++] = 5;
		dhcp_hdr->options[opindex++] = DHO_SUBNET_MASK;
		dhcp_hdr->options[opindex++] = DHO_ROUTERS;
		dhcp_hdr->options[opindex++] = DHO_DOMAIN_NAME_SERVERS;
		dhcp_hdr->options[opindex++] = DHO_DOMAIN_NAME;
		dhcp_hdr->options[opindex++] = DHO_BROADCAST_ADDRESS;
		dhcp_hdr->options[opindex++] = DHO_END;

		int pad = 8;
		for (auto i = 0; i < pad; i++)
			dhcp_hdr->options[opindex++] = 0;

		packsize += opindex;
		uint8_t sourceIP[] = { 0, 0, 0, 0 };
		uint8_t targetIP[] = { 255, 255, 255, 255 };
		IPINFO ipinfo;
		ipinfo.local_port = sock_htons(DHCP_SOURCE_PORT);
		ipinfo.remote_port = sock_htons(DHCP_DEST_PORT);
		ipinfo.local_ip = *(ULONG*)(sourceIP);
		ipinfo.remote_ip = *(ULONG*)(targetIP);
		dhcpClient.sendto(&ipinfo, packet_pad, packsize);
	}
}

void dhcp_recv()
{
	while (1)
	{
		IPINFO dhcpRecv = {};
		uint8_t packet_pad[MAX_UDP_PACKET_SIZE] = {};
		auto recv = dhcpClient.recvfrom(&dhcpRecv, packet_pad, MAX_UDP_PACKET_SIZE);
		if (recv != size_t(-1) &&
			recv > sizeof(DHCP_PACKET))
		{
			//发现了DHCP
			uint8_t  op = packet_pad[0];
			uint8_t  htype = packet_pad[1];
			uint8_t  hlen = packet_pad[2];
			uint8_t  hops = packet_pad[3];
			uint32_t xid = sock_htonl(*(uint32_t *)&packet_pad[4]);
			uint16_t secs = sock_htons(*(uint16_t*)&packet_pad[8]);
			uint16_t flags = sock_htons(*(uint16_t*)&packet_pad[10]);
			uint8_t* ciaddr = &packet_pad[12]; // (Client IP address)
			uint8_t* yiaddr = &packet_pad[16]; // (Your IP address)
			uint8_t* siaddr = &packet_pad[20]; // (Server IP address)
			uint8_t* giaddr = &packet_pad[24]; // (Gateway IP address)
			uint32_t magic = sock_htonl(*(uint32_t*)&packet_pad[236]);

			LOG_DEBUG("op = %d\n", op);
			LOG_DEBUG("htype = %d\n", htype);
			LOG_DEBUG("hlen = %d\n", hlen);
			LOG_DEBUG("hops = %d\n", hops);
			LOG_DEBUG("xid = 0x%0X\n", xid);
			LOG_DEBUG("secs = %d\n", secs);
			LOG_DEBUG("flags = %d\n", flags);
			LOG_DEBUG("ciaddr = %d.%d.%d.%d\n", ciaddr[0], ciaddr[1], ciaddr[2], ciaddr[3]); // (Client IP address)
			LOG_DEBUG("yiaddr = %d.%d.%d.%d\n", yiaddr[0], yiaddr[1], yiaddr[2], yiaddr[3]); // (Your IP address)
			LOG_DEBUG("siaddr = %d.%d.%d.%d\n", siaddr[0], siaddr[1], siaddr[2], siaddr[3]); // (Server IP address)
			LOG_DEBUG("giaddr = %d.%d.%d.%d\n", giaddr[0], giaddr[1], giaddr[2], giaddr[3]); // (Gateway IP address)
			LOG_DEBUG("magic = 0x%0X\n", magic);

			size_t offset = 240;
			size_t remind = recv - 240;
			uint8_t optionData[255] = {};
			uint8_t dhcpType = 0xFF;
			while (offset < remind)
			{
				uint8_t option = packet_pad[offset++];
				uint8_t length = packet_pad[offset++];
				for (int i = 0; i < length; i++)
					optionData[i] = packet_pad[offset++];
				switch (option)
				{
				case DHO_DHCP_MESSAGE_TYPE: // DHCP Message Type
					dhcpType = optionData[0];
					break;
				case DHO_DHCP_LEASE_TIME: // IP Address Lease Time
					ipv4Data.IpAddressLeaseTime = sock_htonl(*(uint32_t *)&optionData[0]);
					break;
				case DHO_DHCP_RENEWAL_TIME: // Renew Time
					ipv4Data.RenewTime = sock_htonl(*(uint32_t *)&optionData[0]);
					break;
				case DHO_DHCP_REBINDING_TIME: // Rebinding Time
					ipv4Data.RebindTime = sock_htonl(*(uint32_t *)&optionData[0]);
					break;
				case DHO_SUBNET_MASK: // Subnet Mask
					for (int i = 0; i < 4; i++)
						ipv4Data.SubnetMask[i] = optionData[i];
					break;
				case DHO_ROUTERS: // Router
					for (int i = 0; i < 4; i++)
						ipv4Data.Gateway[i] = optionData[i];
					LOG_DEBUG("gatewat=");
					dumpIP(ipv4Data.Gateway);
					break;
				case DHO_DOMAIN_NAME_SERVERS: // DNS
					for (int i = 0; i < 4; i++)
						ipv4Data.DomainNameServer[i] = optionData[i];
					// There could be more, but just take one for now
					LOG_DEBUG("dns=");
					dumpIP(ipv4Data.Gateway);
					break;
				case DHO_BROADCAST_ADDRESS: // Broadcast Address
					for (int i = 0; i < 4; i++)
						ipv4Data.BroadcastAddress[i] = optionData[i];
					LOG_DEBUG("BroadCastAddress=");
					dumpIP(ipv4Data.BroadcastAddress);
					break;
				case DHO_END: offset = remind; break;
				}
			}
			if (xid == PendingXID)
			{
				switch (dhcpType)
				{
				case DHCPOFFER: // offer
				{
					LOG_DEBUG("offer\r\n");
					PendingXID = (uint32_t)getTime();
					SendDhcpRequest(yiaddr);
					break;
				}
				case DHCPACK: // ack
				{
					LOG_DEBUG("ack\r\n");
					for (int i = 0; i < 4; i++)
						ipv4Data.Address[i] = yiaddr[i];
					ipv4Data.DataValid = true;
					Ipv4.setLocalIp(ipv4Data.Address);
					const uint8_t* addr = Ipv4.getLocalIp();
					LOG_DEBUG("DHCP got address %d.%d.%d.%d\n", addr[0], addr[1], addr[2], addr[3]);
					return;
					break;
				}
				case DHCPNAK: // nak
					LOG_DEBUG("nak\r\n");
					break;
				default: break;
				}
			}
		}
	}
}

void SendDhcpRequest(uint8_t *RequestIp)
{
	//DHCP第二轮
	uint8_t packet_pad[MAX_UDP_PACKET_SIZE] = {};
	RtlZeroMemory(packet_pad, sizeof(packet_pad));
	auto dhcp_hdr = reinterpret_cast<DHCP_PACKET*>(packet_pad);
	auto packsize = sizeof(DHCP_PACKET) - 4;
	PendingXID = (uint32_t)getTime();
	dhcp_hdr->op = BOOTREQUEST;
	dhcp_hdr->htype = HTYPE_ETHER;
	dhcp_hdr->hlen = ETH_ALEN;
	dhcp_hdr->hop = 0;
	dhcp_hdr->xid = sock_htonl(PendingXID);
	dhcp_hdr->nsec = 0;
	dhcp_hdr->flags = sock_htons(BOOTP_BROADCAST);
	dhcp_hdr->ciaddr = 0;
	dhcp_hdr->yiaddr = 0;
	dhcp_hdr->siaddr = 0;
	dhcp_hdr->giaddr = 0;
	for (auto i = 0; i < 6; i++)
	{
		dhcp_hdr->hard_address[i] = myMac[i];
	}
	dhcp_hdr->magic_dhcp = sock_htonl(DHCP_MAGIC);
	auto opindex = 0;
	//开始OPINDEX
	dhcp_hdr->options[opindex++] = DHO_DHCP_MESSAGE_TYPE;
	dhcp_hdr->options[opindex++] = 1;
	dhcp_hdr->options[opindex++] = DHCPREQUEST;

	dhcp_hdr->options[opindex++] = DHO_DHCP_CLIENT_IDENTIFIER;
	dhcp_hdr->options[opindex++] = 7;
	dhcp_hdr->options[opindex++] = HTYPE_ETHER;
	for (auto i = 0; i < 6; i++)
		dhcp_hdr->options[opindex++] = myMac[i];

	dhcp_hdr->options[opindex++] = DHO_DHCP_SERVER_IDENTIFIER;
	dhcp_hdr->options[opindex++] = 4;
	for (auto i = 0; i < 4; i++)
		dhcp_hdr->options[opindex++] = ipv4Data.Gateway[i];

	dhcp_hdr->options[opindex++] = DHO_DHCP_REQUESTED_ADDRESS;
	dhcp_hdr->options[opindex++] = 4;
	for (auto i = 0; i < 4; i++)
		dhcp_hdr->options[opindex++] = RequestIp[i];

	dhcp_hdr->options[opindex++] = DHO_DHCP_PARAMETER_REQUEST_LIST;
	dhcp_hdr->options[opindex++] = 4;
	dhcp_hdr->options[opindex++] = DHO_SUBNET_MASK;
	dhcp_hdr->options[opindex++] = DHO_ROUTERS;
	dhcp_hdr->options[opindex++] = DHO_DOMAIN_NAME_SERVERS;
	dhcp_hdr->options[opindex++] = DHO_BROADCAST_ADDRESS;
	//结束DHCP
	dhcp_hdr->options[opindex++] = DHO_END;
	int pad = 8;
	for (auto i = 0; i < pad; i++)
		dhcp_hdr->options[opindex++] = 0;

	packsize += opindex;
	uint8_t sourceIP[] = { 0, 0, 0, 0 };
	uint8_t targetIP[] = { 255, 255, 255, 255 };
	IPINFO ipinfo;
	ipinfo.local_port = sock_htons(DHCP_SOURCE_PORT);
	ipinfo.remote_port = sock_htons(DHCP_DEST_PORT);
	ipinfo.local_ip = *(ULONG*)(sourceIP);
	ipinfo.remote_ip = *(ULONG*)(targetIP);
	dhcpClient.sendto(&ipinfo, packet_pad, packsize);
}