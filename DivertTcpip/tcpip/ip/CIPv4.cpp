#include "stdafx.h"

CIPv4 Ipv4;

CIPv4::CIPv4()
{
	bReTxThreadLive = false;
	RtlZeroMemory(&ipv4Addr, sizeof(ipv4Addr));
	uint8_t BroadCastIp[] = { 255, 255, 255, 255 };
	RtlCopyMemory(ipv4Addr.BroadcastAddress, BroadCastIp, 4);
}


CIPv4::~CIPv4()
{
	
}

bool CIPv4::ProcessRx(const uint8_t *packet, size_t packet_len) {
	
	bool bRet = false;
	//解析ip头
	auto buffer = (uint8_t*)packet;
	auto iphdr = reinterpret_cast<ip_header*>(buffer);
	auto Proto = iphdr->proto;
	auto iphdr_len = iphdr->h_len << 2;

	auto newpacket = buffer + iphdr_len;
	auto newlen = packet_len - iphdr_len;

	auto checksum = iphdr->checksum;
	auto checksum2 = ip_checksum(iphdr, iphdr_len);
	if (checksum2!=checksum)
	{
		//LOG_DEBUG("wrong ip header\r\n");
		//dumpIP((uint8_t *)&iphdr->src_ip);
		return bRet;
	}

	//提取targetIp,sourceIp
	auto targetIp = iphdr->dst_ip;
	auto sourceIp = iphdr->src_ip;
	/*LOG_DEBUG("From:");
	dumpIP((uint8_t*)&sourceIp);
	LOG_DEBUG("To:");
	dumpIP((uint8_t*)&targetIp);*/
	if (!isLocalIp((const uint8_t*)&targetIp))
	{
		return bRet;
	}

	if (is_zero_ip(ipv4Addr.Address))
	{
		Ipv4.setLocalIp((uint8_t*)&targetIp);
		LOG_DEBUG("find ip %d.%d.%d.%d\r\n", ipv4Addr.Address[0], ipv4Addr.Address[1],
			ipv4Addr.Address[2], ipv4Addr.Address[3]);
	}

	/*收包:Ip-->对应类型的第一级处理
			-->pool池
				-->internal处理（udp没有，tcp独有的）
					-->具体sock实现*/
	/*发包:具体sock实现-->internal分片处理等等（Tcp独有）
							-->ip层立即发送或者重发队列*/
	switch (Proto)
	{
	case IP_PROTO_TCP:
		//LOG_DEBUG("TCP\r\n");
		bRet = tcpv4.RxProcess(iphdr, packet, packet_len);
		break;
	case IP_PROTO_UDP:
		//LOG_DEBUG("UDP\r\n");
		bRet = udpv4.ProcessRxEx(targetIp, sourceIp, newpacket, newlen);
		break;
	case IP_PROTO_ICMP:
		//LOG_DEBUG("ICMP\r\n");
		//ICMP跟正常的不太一样，需要原始ip头信息
		bRet = icmpv4.ProcessRxEx(targetIp, sourceIp, packet, packet_len);
		break;
	case IP_PROTO_IGMP:
		//LOG_DEBUG("IGMP\r\n");
		break;
	default:
		break;
	}
	
	return bRet;
}
bool CIPv4::TxProcess(const uint8_t *packet, size_t packet_len) {
	//IP层不支持直接对外send了
	return false;
}

bool CIPv4::isLocalIp(const uint8_t *ip) {
	//在不使用DHCP时，我们的Localip是空的，IP地址将作为参数传递到下层UDP/TCP
	if(is_zero_ip(ipv4Addr.Address))
		return true;
	auto ipu32 = *(ULONG*)ip;
	for (auto _ipAddr : ipList)
	{
		if (ipu32 == _ipAddr)
			return true;
	}

	for (auto i = 0; i < 4; i++)
	{
		if (ip[i] != ipv4Addr.Address[i])
			return false;
	}

	return true;
}

void CIPv4::setLocalIp(uint8_t *_ip)
{
	//如果使用dhcp方式，则会使用这里
	RtlCopyMemory(ipv4Addr.Address, _ip, 4);
}

bool CIPv4::isBroadCastIp(const uint8_t *ip)
{
	
	for (auto i = 0; i < 4; i++)
	{
		if (ip[i] != ipv4Addr.BroadcastAddress[i])
			return false;
	}
	return true;
}
bool CIPv4::TxPacket(const uint32_t dst_ip, const uint32_t src_ip, uint8_t proto, const uint8_t *packet, size_t packet_len)
{
	//return false;
	auto pnew_packet = (uint8_t *)malloc(packet_len + 20);
	if (!pnew_packet)
	{
		return false;
	}
	RtlZeroMemory(pnew_packet, packet_len + 20);
	//直接Build出Ip包
	auto ip_hdr = reinterpret_cast<ip_header*>(pnew_packet);
	ip_hdr->version = 4;
	ip_hdr->h_len = 20 >> 2;
	ip_hdr->tos = 0;
	ip_hdr->len = sock_htons((unsigned short)(packet_len+20));
	ip_hdr->ident = 9910;			/* ip序号 */
	ip_hdr->flags_offset = 0;
	ip_hdr->df = 1;
	ip_hdr->ttl = 128;
	ip_hdr->proto = proto;		/* 子协议  */
	ip_hdr->dst_ip = dst_ip;
	ip_hdr->src_ip = src_ip;
	auto checksum = ip_checksum(ip_hdr, 20);
	ip_hdr->checksum = checksum;
	//IP頭ok
	RtlCopyMemory(pnew_packet + 20, packet, packet_len);
	WINDIVERT_ADDRESS _Addr = {};
	_Addr.Direction = WINDIVERT_DIRECTION_OUTBOUND;
	auto bRet = CPacketIo::getInstance().sendpacket(&_Addr,
		(PVOID)pnew_packet, packet_len + 20);
	if (!bRet)
	{
			ReTxPacket(dst_ip, pnew_packet, packet_len + 20);
			return bRet;
	}
	free(pnew_packet);
	return bRet;
}

void CIPv4::ReTxPacket(uint32_t _ip, uint8_t*ippacket, size_t _ippacket_size)
{
	IPRETX retxpacket = {};
	retxpacket.ipAddr = _ip;
	retxpacket.buffer = ippacket;
	retxpacket.lens = _ippacket_size;
	reTxQueue.push(retxpacket);
	if (!bReTxThreadLive)
	{
		bReTxThreadLive = true;
		//开个新线程
		auto thread = std::thread(std::bind(&CIPv4::ReTxThread, this));
		thread.detach();
	}
}

void CIPv4::ReTxThread()
{
	Sleep(5);
	while (!reTxQueue.empty())
	{
		auto Retxpacket = reTxQueue.front();
		reTxQueue.pop();
		bool bSent = false;
		WINDIVERT_ADDRESS _Addr = {};
		_Addr.Direction = WINDIVERT_DIRECTION_OUTBOUND;
		bSent = CPacketIo::getInstance().sendpacket(&_Addr,
			(PVOID)Retxpacket.buffer, Retxpacket.lens);
		if(!bSent)
		{
			//加入IP包重发队列
			reTxQueue.push(Retxpacket);
		}
		else {
			free(Retxpacket.buffer);
		}
		Sleep(1);
	}
	bReTxThreadLive = false;
}