#include "stdafx.h"
CTcpv4 tcpv4;

CTcpv4::CTcpv4()
{
}


CTcpv4::~CTcpv4()
{
}

bool CTcpv4::RxProcess(struct ip_header*iphdr, const uint8_t *packet, size_t packet_size)
{
	//////////////////////////////////////////////////////////////////////////
	/*非常复杂的TCP就在这里开始了*/
	/*$killvxk 在2018.4.24 14:30留下这个注释*/
	//////////////////////////////////////////////////////////////////////////
	/*
	1.Tcp收包不要做checksum验证，不为什么。
	2.RFC文档那些文字不错，但千万别完全相信，
	3.真照着RFC一个样实现的TCPIP栈一个都没有
	4.Tcp居然没有有简单的Lite协议版本，看来自己造自行车了
	*/
	//////////////////////////////////////////////////////////////////////////
	/*上面都是吐槽，下面才是真的干活*/
	/*TCP复杂问题：
	1)超时重发
	2)Accept
	3)滑动窗口 收发组包*/
	//////////////////////////////////////////////////////////////////////////
	//首先把包给pool池
	//Tcp的pool池必须异步，因为操作复杂，同步效率低
	//pool池将包存入pool池队列里
	//pool池异步进行查询工作是否有internal层监听了对应的
	//本地端口，远程端口，远程地址的internal
	//如果有就直接交给相应的internal层
	//没有直接返回就行了
	uint16_t iphdr_len = iphdr->h_len << 2;
	CSockTcpPool::getInstance().RxProcess(iphdr, iphdr_len, (uint8_t*)packet, packet_size);
	return true;
}
