#pragma once
class CTcpv4
{
public:
	CTcpv4();
	~CTcpv4();
public:
	bool RxProcess(struct ip_header*iphdr, const uint8_t *packet, size_t packet_size);
};

extern CTcpv4 tcpv4;
