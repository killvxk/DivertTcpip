#pragma once
class CICMPv4
{
public:
	CICMPv4();
	~CICMPv4();
public:
	bool TxProcess(const uint8_t *packet, size_t packet_len);
	bool ProcessRxEx(const uint32_t dstIp, const uint32_t srcIp, const uint8_t *packet, size_t packet_len);
};

extern CICMPv4 icmpv4;