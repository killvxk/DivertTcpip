#include "stdafx.h"
#include "CICMPv4.h"

CICMPv4 icmpv4;

CICMPv4::CICMPv4()
{
}


CICMPv4::~CICMPv4()
{
}

bool CICMPv4::ProcessRxEx(const uint32_t dstIp, const uint32_t srcIp, const uint8_t *packet, size_t packet_len)
{
	
	bool ret = false;
	auto buff = (uint8_t*)packet;
	auto iphdr = reinterpret_cast<ip_header *>(buff);
	auto iphdr_len = iphdr->h_len << 2;
	auto icmphdr = reinterpret_cast<icmp_header*>(buff+iphdr_len);
	auto icmplen = packet_len - iphdr_len;
	LOG_DEBUG("icmp packet size=%lld\r\n", icmplen);
	auto type = icmphdr->Type;
	auto code = icmphdr->Code;
	auto sum = icmp_checksum(icmphdr, (uint16_t)icmplen);
	if (sum!=icmphdr->checksum)
	{
		LOG_DEBUG("icmp sum wrong\r\n");
		return false;
	}
	LOG_DEBUG("ICMP Type=%x Code=%x size=%lld\r\n", type, code, icmplen);
	switch (type)
	{
	case 8:
		LOG_DEBUG("Proc Ping\r\n");
		icmphdr->Type = 0;
		//if(packet_len >8)
		//	memset(icmphdr->data, 0x41, packet_len - 8);
		icmphdr->checksum = icmp_checksum(icmphdr, (uint16_t)icmplen);
		ret = Ipv4.TxPacket(srcIp, dstIp,IP_PROTO_ICMP, (const uint8_t *)icmphdr, icmplen);
		break;
	case 0:
		LOG_DEBUG("reply");
		//Reply处理，送ip头的TTL Seq ID给下面
		ret = CSocketIcmpPool::getInstance().processRx(dstIp, icmphdr->id, icmphdr->sqe, iphdr->ttl);
		break;
	default:
		break;
	}
	return ret;
}
bool CICMPv4::TxProcess(const uint8_t *packet, size_t packet_len) {
	return false;
}

