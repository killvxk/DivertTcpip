#pragma once
bool is_zero_ip(uint8_t *_ip);
bool is_same_mac_address(uint8_t *_addr, uint8_t *_addr2);
void dumpMac(uint8_t *_mac);
void dumpIP(uint8_t *ip);
//RFC标准checksum
unsigned short rfc_checksum(void* proto_header, int header_len, void *buf, int buf_size);

//ip头checksum
unsigned short ip_checksum(struct ip_header* ipheader, unsigned short ip_len);
//icmp的checksum
unsigned short icmp_checksum(struct icmp_header* icmpheader, unsigned short icmp_len);
//tcp的checksum
unsigned short tcp_checksum(struct tcp_header* tcpheader, 
	uint16_t tcp_header_len,
	struct ip_header*ipheader,
	void *packet,
	size_t packet_len);

unsigned long sock_htonl(unsigned long data);
unsigned short sock_htons(unsigned short data);

#define sock_ntohl(d) sock_htonl(d)
#define sock_ntohs(d) sock_htons(d)

int sock_rand();
int64_t getTime();
