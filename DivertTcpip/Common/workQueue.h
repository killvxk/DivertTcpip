#pragma once
typedef struct _WORKQ_ITEM_
{
	LIST_ENTRY List;
	uint8_t insertCount;
	LONGLONG Time;
	PVOID Context;
	std::function<void(PVOID)>m_handler;
}WORKQ_ITEM,*PWORKQ_ITEM;
enum NORMAL_DEFINE_FOR_WORKQ
{
	MAX_WORKQ_LIST = 8,
	MAX_WORKQ_COUNT=10,
};
class WorkQueue:public Singleton<WorkQueue> {
public:
	WorkQueue();
	~WorkQueue();
private:
	spinlock_mutex m_lock_workqueue[MAX_WORKQ_LIST];
	LIST_ENTRY m_WorkQueue[MAX_WORKQ_LIST];
	size_t m_count[MAX_WORKQ_LIST];
	uint8_t InsertIndex;
	std::thread m_workThread[MAX_WORKQ_LIST];
private:
	void ExpWorkerThread(uint8_t WorkListIndex);
public:
	void InitializeWorkItem(PWORKQ_ITEM WorkItem,LONGLONG Time, std::function<void(PVOID)> hanlder, PVOID Context);
	void ExQueueWorkItem(PWORKQ_ITEM WorkItem);
	void KillWorkItem(PWORKQ_ITEM WorkItem);
};