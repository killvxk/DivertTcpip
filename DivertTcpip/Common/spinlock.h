#pragma once
class spinlock_mutex
{
	std::atomic_flag flag;
public:
	spinlock_mutex() {
		flag._My_flag = 0;
	}
	void lock()
	{
		while (flag.test_and_set(std::memory_order_acquire))
		{
			YieldProcessor();
		}
	}
	void unlock()
	{
		flag.clear(std::memory_order_release);
	}
};
