#include "stdafx.h"
#include "Common.h"

WorkQueue::WorkQueue()
{
	RtlZeroMemory(m_count, sizeof(m_count));
	InsertIndex = 0;
	RtlZeroMemory(m_WorkQueue, sizeof(m_WorkQueue));
	for (auto i = 0; i < ARRAYSIZE(m_WorkQueue); i++)
	{
		ntdll::InitializeListHead(&m_WorkQueue[i]);
		m_workThread[i] = std::thread(std::bind(&WorkQueue::ExpWorkerThread, this, std::placeholders::_1), i);
		m_workThread[i].detach();
	}
}
WorkQueue::~WorkQueue() {
	for (auto i=0;i<ARRAYSIZE(m_WorkQueue);i++)
	{
		auto handle = m_workThread[i].native_handle();
		TerminateThread(handle, 0);
		m_lock_workqueue[i].lock();
		while (!ntdll::IsListEmpty(&m_WorkQueue[i]))
		{
			ntdll::RemoveTailList(&m_WorkQueue[i]);
		}
		m_lock_workqueue[i].unlock();
	}
}

void WorkQueue::ExpWorkerThread(uint8_t WorkListIndex)
{
	auto hThread = GetCurrentThread();
	while (1)
	{
		m_lock_workqueue[WorkListIndex].lock();
		auto ns = WaitForSingleObject(hThread, 1);//等待1ms
		if (ns == WAIT_TIMEOUT)
		{
			auto List = &m_WorkQueue[WorkListIndex];
			auto next = List->Flink;
			if (!ntdll::IsListEmpty(List)) {
				
				for (next;next!=List;)
				{
					auto workitem = (PWORKQ_ITEM)(next);
					auto back = next->Flink;
					InterlockedIncrement64(&workitem->Time);
					if (workitem->Time == 0)
					{
						ntdll::RemoveEntryList(next);
						m_count[WorkListIndex]--;
						auto thread = std::thread(workitem->m_handler, workitem->Context);
						thread.detach();
					}
					next = back;
				}
			}
		}
		m_lock_workqueue[WorkListIndex].unlock();
		if (ns != STATUS_TIMEOUT) {
			break;
		}
	}
}

void WorkQueue::ExQueueWorkItem(PWORKQ_ITEM WorkItem)
{
	if (WorkItem&&WorkItem->m_handler
		&&WorkItem->Time <= 0)
	{
		//插入操作
		ntdll::InitializeListHead(&WorkItem->List);
		
		if (m_count[InsertIndex] >= MAX_WORKQ_COUNT)
		{
			//找最小的一个
			for (auto i=0;i<ARRAYSIZE(m_count);i++)
			{
				if (m_count[i] < m_count[InsertIndex])
				{
					InsertIndex = i;
				}
			}
		}
		m_lock_workqueue[InsertIndex].lock();
		m_count[InsertIndex]++;
		WorkItem->insertCount = InsertIndex;
		ntdll::InsertTailList(&m_WorkQueue[InsertIndex],
			&WorkItem->List);
		m_lock_workqueue[InsertIndex].unlock();
	}
	
}

void WorkQueue::InitializeWorkItem(PWORKQ_ITEM WorkItem, LONGLONG Time, std::function<void(PVOID)> hanlder, PVOID Context)
{
	if (!WorkItem ||
		!hanlder)
	{
		return;
	}
	RtlZeroMemory(WorkItem, sizeof(WORKQ_ITEM));
	ntdll::InitializeListHead(&WorkItem->List);
	WorkItem->m_handler = hanlder;
	WorkItem->Context = Context;
	WorkItem->Time = Time;
	return;
}

void WorkQueue::KillWorkItem(PWORKQ_ITEM WorkItem)
{
	if (WorkItem)
	{
		auto killIndex = WorkItem->insertCount;
		if (killIndex < MAX_WORKQ_LIST)
		{
			m_lock_workqueue[killIndex].lock();
			if (WorkItem->Time < 0)
			{
				//计时器没有结束
				m_count[killIndex]--;
				ntdll::RemoveEntryList(&WorkItem->List);
			}
			m_lock_workqueue[killIndex].unlock();
		}
	}
}