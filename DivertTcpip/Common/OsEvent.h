#pragma once
class osEvent
{
public:
	osEvent() {
		Event = CreateEvent(NULL, TRUE, FALSE, NULL);
	}
	~osEvent() {
		if (Event)
			CloseHandle(Event);
	}
private:
	HANDLE Event;
public:
	bool Wait(LONG Time) {
		auto status = WaitForSingleObject(Event, Time);
		if (status == WAIT_TIMEOUT)
			return true;
		return false;
	}
	bool Wait() {
		auto status = WaitForSingleObject(Event, INFINITE);
		if (status == WAIT_OBJECT_0)
			return true;
		return false;
	}
	bool Set() {
		if(Event)
			return SetEvent(Event);
		return false;
	}
	bool Reset() {
		if(Event)
			return ResetEvent(Event);
		return false;
	}
	void clean() {
		CloseHandle(Event);
		Event = NULL;
	}
};