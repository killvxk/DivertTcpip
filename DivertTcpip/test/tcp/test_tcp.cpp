#include "stdafx.h"

void TestTcp0()
{
	Sleep(10000);
	CSocketTcp tcp;
	tcp.TcpBind(9999);
	tcp.TcpLisent();
	auto new_cline = tcp.Accept();
}

void TestTcp()
{
	uint8_t IP[] = { 192,168,1,104 };
	uint8_t baidu[] = { 61,135,169,121 };
	//Sleep(10000);
	CSocketTcp tcp;
	//tcp.TcpBind(5555);
	if (!tcp.TcpConnect(*(uint32_t*)IP, 9999)) {
		LOG_DEBUG("Failed\r\n");
		return;
	}
	LOG_DEBUG("connect ok\r\n");
	char httpget[] = "GET / HTTP/1.1\r\n"
		"Connection:keep-alive\r\n"
		"Keep-Alive:10000000\r\n\r\n";
	auto send_size = tcp.blockSend((const uint8_t*)httpget, strlen(httpget));
	if (send_size == strlen(httpget))
	{
		LOG_DEBUG("send ok\r\n");
		while (1)
		{
			char recv[1000] = {};
			auto recvsize = tcp.blockRecv((uint8_t*)recv, 1000);
			if (recvsize != size_t(-1) &&recvsize>0) {
				LOG_DEBUG("recv=%s\r\n", recv);
			}
			else {
				tcp.blockSend((const uint8_t*)httpget, strlen(httpget));
				Sleep(500);
			}
			
		}
	}
	else {
		LOG_DEBUG("send failed %lld\r\n", send_size);
	}
	
}