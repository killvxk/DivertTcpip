#include "stdafx.h"

void TestUdp()
{
	uint8_t pp[100] = {};
	uint8_t sendstring[100+13] = "hello world ";
	CSocketUDP udp;
	if (udp.bindport(18000))
	{
		LOG_DEBUG("bind port 18000 ok\r\n");
		while (1)
		{
			IPINFO _info = {};
			auto recv_size = udp.recvfrom(&_info, (const uint8_t*)pp, 100);
			if (recv_size != 0 && recv_size != size_t(-1))
			{
				LOG_DEBUG("recv %s\r\n", (char*)pp);
				RtlCopyMemory(sendstring + 12, pp, recv_size);
				sendstring[recv_size + 13] = 0;
				udp.sendto(&_info, (const uint8_t*)sendstring, recv_size+13);
			}
		}
	}
}