#include "stdafx.h"

void PingTest(uint8_t *Pingip)
{
	uint8_t xttl = 0;
	uint32_t timer = 0;
	CSocketIcmp ping;
	ping.ping_ip(*(uint32_t*)Pingip);
	ping.get_reply(xttl, timer);
	LOG_DEBUG("ping %d.%d.%d.%d time=%dms ttl=%d\r\n", Pingip[0], Pingip[1],
		Pingip[2], Pingip[3], timer, xttl);
}

void Testping()
{
	Sleep(5000);
	uint8_t ip[] = { 192,168,1,1 };
	for (auto i = 0; i < 8; i++)
	{
		PingTest(ip);
	}
}