// DivertTcpip.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "test/Test.h"

bool RxPacket(PWINDIVERT_ADDRESS _addr, PVOID _packet, size_t _size)
{
	//LOG_DEBUG("收包测试\r\n");
	Ipv4.ProcessRx((const uint8_t*)_packet, _size);
	return false;
}

int set_ips(){
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;
	wVersionRequested = MAKEWORD(1, 1);
	err = WSAStartup(wVersionRequested, &wsaData);//initiate the ws2_32.dll and match the version
	if (err != 0)
	{
		return -1;
	}
	char hostname[MAX_PATH] = {};
	int ret = gethostname(hostname, sizeof(hostname));
	if (ret == -1) {
		LOG_DEBUG("failed\r\n");
		return -1;
	}
	struct hostent *hent;
	hent = gethostbyname(hostname);
	if (NULL == hent) {
		LOG_DEBUG("failed byname\r\n");
		return -1;
	}
	//直接取h_addr_list列表中的第一个地址h_addr
	auto ip = (((struct in_addr*)hent->h_addr)->s_addr);
	//int i;
	auto count = 0;
	for(count=0; hent->h_addr_list[count]; count++) {
	    uint32_t u = (((struct in_addr*)hent->h_addr_list[count])->s_addr);
		Ipv4.addIp(u);
	}
	if (count == 1)
	{
		//只有一个Adapter这就比较好
		Ipv4.setLocalIp((uint8_t*)(&ip));
	}
	return 0;
}

int main()
{
	if (CPacketIo::getInstance().init())
	{
		LOG_DEBUG("init ok\r\n");
		set_ips();
		CPacketIo::getInstance().set_recv_handler(RxPacket);
		if (CPacketIo::getInstance().StartLoop())
		{
			LOG_DEBUG("Start Loop ok\r\n");
			while (is_zero_ip((uint8_t*)Ipv4.getLocalIp()))
			{
				Sleep(10);
			}
			//这里获取了本地ip可以搞事情了
			//Testping();
			//TestUdp();
			//TestTcp();
			while (1)
			{
				Sleep(5000);
			}
		}
	}
    return 0;
}

