#pragma once
#include "WinDivert.h"
class CPacketIo:public Singleton<CPacketIo>
{
public:
	CPacketIo();
	~CPacketIo();
public:
	bool init();
	void set_recv_handler(std::function<bool(PWINDIVERT_ADDRESS, PVOID, size_t)> _recvhandler);
	bool sendpacket(PWINDIVERT_ADDRESS _addr, PVOID _packet, size_t _size);
	bool StartLoop();
private:
	std::thread LoopThread;
	bool bLoopDone;
	bool bStartLoop;
	void LoopRoutine();
private:
	HANDLE _RecvHandle;
	HANDLE _SendHandle;
	std::function<bool(PWINDIVERT_ADDRESS, PVOID, size_t)> mRecvHandler;
private:
	bool binit;
	HMODULE hLoadMod;
	decltype(&WinDivertOpen) pfnWinDivertOpen;
	decltype(&WinDivertClose) pfnWinDivertClose;
	decltype(&WinDivertRecv) pfnWinDivertRecv;
	decltype(&WinDivertSend) pfnWinDivertSend;
	decltype(&WinDivertSetParam) pfnWinDivertSetParam;
};

