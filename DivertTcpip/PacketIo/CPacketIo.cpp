#include "stdafx.h"
#include "CPacketIo.h"


CPacketIo::CPacketIo()
{
	pfnWinDivertClose = nullptr;
	pfnWinDivertRecv = nullptr;
	pfnWinDivertSend = nullptr;
	pfnWinDivertOpen = nullptr;
	pfnWinDivertSetParam = nullptr;
	mRecvHandler = nullptr;
	binit = false;
	bLoopDone = false;
	bStartLoop = false;
	hLoadMod = nullptr;
}


CPacketIo::~CPacketIo()
{
	if (binit)
	{
		bLoopDone = true;
		if (bStartLoop)
		{
			LoopThread.join();
		}
		pfnWinDivertClose(_SendHandle);
		pfnWinDivertClose(_RecvHandle);
		FreeLibrary(hLoadMod);
	}

}
#define GET_DIVERT_API(name) pfn##name = reinterpret_cast<decltype(&##name)>(GetProcAddress(hLoadMod, #name))
bool CPacketIo::init()
{
	if (!binit)
	{
		do
		{
			hLoadMod = LoadLibrary(_T("WinDivert.dll"));
			if (!hLoadMod)
			{
				LOG_DEBUG("Load Windivert failed\r\n");
				break;
			}
			GET_DIVERT_API(WinDivertOpen);
			GET_DIVERT_API(WinDivertClose);
			GET_DIVERT_API(WinDivertRecv);
			GET_DIVERT_API(WinDivertSend);
			GET_DIVERT_API(WinDivertSetParam);

			if (!pfnWinDivertOpen || !pfnWinDivertClose || !pfnWinDivertRecv || !pfnWinDivertSend || !pfnWinDivertSetParam)
			{
				LOG_DEBUG("Get Api failed\r\n");
				break;
			}

			_RecvHandle = pfnWinDivertOpen("inbound and !loopback and ip",
				WINDIVERT_LAYER_NETWORK, 0, WINDIVERT_FLAG_SNIFF);
			if (_RecvHandle == INVALID_HANDLE_VALUE)
			{
				LOG_DEBUG("recv handle open failed\r\n");
				break;
			}

			if (!pfnWinDivertSetParam(_RecvHandle, WINDIVERT_PARAM_QUEUE_TIME, 8000))
			{
				LOG_DEBUG("set queue time failed\r\n");
				break;
			}

			if (!pfnWinDivertSetParam(_RecvHandle, WINDIVERT_PARAM_QUEUE_SIZE, 33554432))
			{
				LOG_DEBUG("set queue size failed\r\n");
				break;
			}

			_SendHandle = pfnWinDivertOpen("false", WINDIVERT_LAYER_NETWORK, 0, 0);
			if (_SendHandle == INVALID_HANDLE_VALUE)
			{
				LOG_DEBUG("send handle open failed\r\n");
				break;
			}
			binit = true;
		} while (0);
	}
	return binit;
}

void CPacketIo::set_recv_handler(std::function<bool(PWINDIVERT_ADDRESS, PVOID, size_t)> _recvhandler)
{
	mRecvHandler = _recvhandler;
}

bool CPacketIo::sendpacket(PWINDIVERT_ADDRESS _addr, PVOID _packet, size_t _size)
{
	if (!binit)
		return false;
	UINT write = 0;
	if (pfnWinDivertSend(_SendHandle, _packet, (UINT)_size, _addr, &write))
	{
		if (write==(UINT)_size)
		{
			return true;
		}
		else {
			LOG_DEBUG("packet part send send_size = %u\r\n", write);
		}
	}
	LOG_DEBUG("send failed\r\n");
	return false;
}

bool CPacketIo::StartLoop()
{
	if (!binit)
	{
		return false;
	}
	if (bStartLoop)
	{
		return false;
	}
	if (!mRecvHandler)
	{
		return false;
	}
	LoopThread = std::thread(std::bind(&CPacketIo::LoopRoutine, this));
	if (LoopThread.joinable())
	{
		bStartLoop = true;
		return true;
	}
	return false;
}
void CPacketIo::LoopRoutine()
{
	while (!bLoopDone)
	{
		//收包
		//调用RecvHandler
		WINDIVERT_ADDRESS addr = {};
		UINT packet_len = 0;
		uint8_t packet[1600] = {};
		if (!pfnWinDivertRecv(_RecvHandle, packet, sizeof(packet), &addr, &packet_len))
			continue;

		auto no_reinject = mRecvHandler(&addr,(PVOID)packet,(size_t)packet_len);
		
		/*
		//我们工作在Sniff模式，Drop模式才用
		if (!no_reinject)
		{
			pfnWinDivertSend(_RecvHandle, packet, packet_len, &addr);
		}*/
	}
}